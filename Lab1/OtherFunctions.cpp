#include "OtherFunctions.h"
#include "Calc.h"
//===============================Other Functions================================
bool CheckVarName(const UnicodeString &ch )
{
  return !( ch == "" || isdigit(ch[1]) );
}
bool IsKeyWord(const UnicodeString &ch)
{
  return SearchIn( KeyWords , ch ) >= 0;
}
template <class T>
bool InVect( const T &val , const std::vector<T> &vect )
{
  for( int i = 0; i < vect.size(); i++ )
  {
	if( val == vect[i] ) return true;
  }
  return false;
}
bool IsDataType(const UnicodeString &ch)
{
  return SearchIn( DataTypes , ch ) >= 0;
}

bool IsSomeSym(const UnicodeString &ch)
{
  for( int i = 1; i <= Symbols.Length(); i++ )
	if( ch == Symbols[i] ) return true;
  return false;
}

bool isdigit( const wchar_t &c )
{
  for(char i = '0'; i <= '9'; i++)
	if( c == i ) return true;
  return false;
}
bool IsSomeOp( const UnicodeString &ch )
{
  return SearchIn( "== = <= >= || && > < ! != " , ch ) >= 0;
}

int max(int a, int b)
{
    return (a > b) ? a : b;
}

void badCharHeuristic(char *str, int size, int badchar[NO_OF_CHARS])
{
	int i;
	for (i = 0; i < NO_OF_CHARS; i++)
		badchar[i] = -1;
    for (i = 0; i < size; i++)
        badchar[(int) str[i]] = i;
}

int search(char *txt, char *pat)
{
    int m = strlen(pat);
	int n = strlen(txt);
	int badchar[NO_OF_CHARS];
	badCharHeuristic(pat, m, badchar);
	int s = 0;
    while (s <= (n - m))
    {
		int j = m - 1;
		while (j >= 0 && pat[j] == txt[s + j])
			j--;
		if (j < 0)
		{
			return s;
			s += (s + m < n) ? m - badchar[txt[s + m]] : 1;
		}
		else
			s += max(1, j - badchar[txt[s + j]]);
	}
	return -1;
}

int SearchIn( const UnicodeString &text , const UnicodeString &word)
{
  return search(AnsiString(" "+text+" ").c_str() , AnsiString(" "+word+" ").c_str());
}

TType GetTType( const UnicodeString &TypeSt )
{
  if( IsKeyWord(TypeSt) ) 	return T_KEY_WORD_;
  if( IsSomeSym(TypeSt) ) 	return T_SOME_SYM_;
  if( IsDataType(TypeSt) ) 	return T_DATA_TYPE_;
  return T_VOID_;
}

UnicodeString TTypeToString( const TType &SomeType )
{
  switch( SomeType )
  {
	case  T_KEY_WORD_ 	: return "T_KEY_WORD_";
	case  T_DATA_TYPE_ 	: return "T_DATA_TYPE_";
	case  T_SOME_SYM_ 	: return "T_SOME_SYM_";
	case  T_VOID_ 		: return "T_VOID_";
  }
  return "UNKNOWN";
}

UnicodeString LTypeToString( const LType &SomeType )
{
  switch( SomeType )
  {
	case  KEY_WORD 		: return "KEY_WORD";
	case  DATA_TYPE 	: return "DATA_TYPE";
	case  VARIABLE 		: return "VARIABLE";
	case  SOME_SYM	 	: return "SOME_SYM";
	case  SOME_OP 		: return "SOME_OP";
	case  SEPSYM 		: return "SEPSYM";
	case  VALUE 		: return "VALUE";
	case  THREAD_IDENT 	: return "THREAD_IDENT";
  }
  return "UNKNOWN";
}

DWORD _stdcall FindText( void *p )
{
  sData &SearchData =  *((sData*)p);
  Find( SearchData.SomeExp , SearchData.Variables );
  return 0;
}
//------------------------------------------------------------------------------
UnicodeString Calculate(const UnicodeString &ResType , std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{

  if( ResType == "int")   return CalculateInt   ( SomeExp , Variables );
  if( ResType == "float") return CalculateFloat ( SomeExp , Variables );
  if( ResType == "bool")  return CalculateBool  ( SomeExp , Variables );
  if( ResType == "char")  return CalculateChar  ( SomeExp , Variables );
  if( ResType == "string")return CalculateString( SomeExp , Variables );
  return "";
}
//------------------------------------------------------------------------------
UnicodeString CalculateInt( std::list<Lexeme> &SomeExp , std::vector< SomeVar > &Variables )
{
  std::vector< std::string > Exp;

  while( SomeExp.size() )
  {
    if( SomeExp.front().GetLType() == VARIABLE )
	{
	  bool f = true;
	  for( int i = 0; i < Variables.size();i++ )
	  {
		if( SomeExp.front().GetValue() == Variables[i].GetName() && Variables[i].GetType() == "int" )
		{
		  SomeExp.pop_front();
		  SomeExp.push_front(Lexeme( Variables[i].GetValue() , VALUE ));
		  f = false;
		}
        if( SomeExp.front().GetValue() == Variables[i].GetName() && Variables[i].GetType() == "bool" )
		{
		  SomeExp.pop_front();
		  if( Variables[i].GetValue() == "true")
			SomeExp.push_front( Lexeme("1" , VALUE) );
		  else
			SomeExp.push_front( Lexeme("0" , VALUE) );
		  f = false;
		}
	  }
	  if(f) throw 1;
	}
	if( !IsNumber(AnsiString( SomeExp.front().GetValue() ).c_str()) )
	  if( !ChInString( AnsiString( SomeExp.front().GetValue() ).c_str()[0] , "()+-*/^") )
		throw 1;
	Exp.push_back( AnsiString( SomeExp.front().GetValue() ).c_str() );
	SomeExp.pop_front();
  }
  return UnicodeString(Clc( Exp ).c_str()) ;
}
//------------------------------------------------------------------------------
UnicodeString CalculateBool  ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{
  std::vector< std::string > Exp;
  while( SomeExp.size() )
  {
    if( SomeExp.front().GetLType() == VARIABLE )
	{
	  bool f = true;
	  for( int i = 0; i < Variables.size();i++ )
	  {
		if( SomeExp.front().GetValue() == Variables[i].GetName() && Variables[i].GetType() == "int" )
		{
		  SomeExp.pop_front();
		  SomeExp.push_front(Lexeme( Variables[i].GetValue() , VALUE ));
		  f = false;
		}
		if( SomeExp.front().GetValue() == Variables[i].GetName() && Variables[i].GetType() == "bool" )
		{
		  SomeExp.pop_front();
		  if( Variables[i].GetValue() == "true")
			SomeExp.push_front( Lexeme("1" , VALUE) );
		  else
			SomeExp.push_front( Lexeme("0" , VALUE) );
		  f = false;
		}
	  }
	  if(f) throw 1;
	}
	if( !IsNumber(AnsiString( SomeExp.front().GetValue() ).c_str()) )
	  if( !ChInString( AnsiString( SomeExp.front().GetValue() ).c_str()[0] , "()><=+-*/^") || SomeExp.front().GetValue() == "=" )
		throw 1;
	Exp.push_back( AnsiString( SomeExp.front().GetValue() ).c_str() );
	SomeExp.pop_front();
  }
  return UnicodeString(ClcB( Exp ).c_str()) ;
}
//------------------------------------------------------------------------------
UnicodeString CalculateChar( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{
  if( SomeExp.size() < 3 ) throw 1;
  UnicodeString Res;
  if( SomeExp.front().GetValue() != "\"" ) throw 1;
  SomeExp.pop_front();
  if( SomeExp.front().GetValue().Length() > 1 ) throw 1;
  Res = SomeExp.front().GetValue();
  SomeExp.pop_front();
  if( SomeExp.front().GetValue() != "\"" ) throw 1;
  return Res;
}
//------------------------------------------------------------------------------
UnicodeString CalculateFloat ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{
  if( SomeExp.size() == 1 ) return SomeExp.front().GetValue();
  UnicodeString Res = "";
  while( SomeExp.size() )
  {
	Res += SomeExp.front().GetValue() +  " ";
	SomeExp.pop_front();
  }
  return Res;
}
//------------------------------------------------------------------------------
UnicodeString CalculateString( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{

  UnicodeString Res = "";
  int L = 0;
  while( SomeExp.size() )
  {
	if( SomeExp.front().GetValue() == "\"" )
	{
	  SomeExp.pop_front();
	  L++;
	  continue;
	}
	if( SomeExp.front().GetLType() == VARIABLE )
	{
	  bool f = true;
	  for( int i = 0; i < Variables.size(); i++ )
	  {
		if( Variables[i].GetName() == SomeExp.front().GetValue())
		{
		  SomeExp.pop_front();
		  SomeExp.push_front( Lexeme(Variables[i].GetValue(),VALUE) );
		  f = false;
		  L = 2;
		}
	  }
	  if(f) throw 1;
	}
	if( SomeExp.front().GetLType() == VALUE )
	{
	  Res += SomeExp.front().GetValue();
	  SomeExp.pop_front();
	  continue;
	}
	throw 1;
  }
  if( L < 2 ) throw 1;
  return Res;
}
//------------------------------------------------------------------------------
int FindInText( const UnicodeString &Word , const UnicodeString &Text , const int &pos )
{
  if(pos < 1) return -1;
  int j = 1,l = 1;
  for( int i = pos; i <= Text.Length() - Word.Length()+1; i++ )
  {
	j = 1; l = i;
	while( j <= Word.Length() && l <= Text.Length() && Word[j] == Text[l] )
	{
	  l++; j++;
	}
	if( j == Word.Length() + 1 ) return i;
  }
  return -1;
}
//------------------------------------------------------------------------------
bool Find( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables )
{
  std::list<UnicodeString> Word;
  std::vector<int> OpSymPos;
  OpSymPos.reserve( SomeExp.size() );
  UnicodeString Text;
  bool b = true;
  int i = -1;
  Text = "";
  SomeExp.pop_front();
  while( SomeExp.front().GetValue() != "in" )
  {
	b = true; i++;
	if( SomeExp.front().GetValue() == "[" )
	{
	  SomeExp.pop_front();
	  std::list<Lexeme> Exp;
	  while( SomeExp.size() && SomeExp.front().GetValue() != "]" )
	  {
		Exp.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  Word.push_back( Calculate( "int" , Exp , Variables ) );
	  continue;
	}
	if( SomeExp.front().GetValue() == "<" )
	{
	  SomeExp.pop_front();
	  std::list<Lexeme> Exp;
	  while( SomeExp.size() && SomeExp.front().GetValue() != ">" )
      {
		Exp.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  Word.push_back( UnicodeString(char(Calculate( "int" , Exp , Variables ).ToInt())) );
	  continue;
	}
	if( SomeExp.front().GetValue() == "\"" )
	{
	  SomeExp.pop_front();
	  while( SomeExp.size() && SomeExp.front().GetValue() != "\"" )
	  {
		Word.push_back(SomeExp.front().GetValue());
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  continue;
	}
	if( SomeExp.front().GetValue() == "\?" || SomeExp.front().GetValue() == "*" )
	{
	  Word.push_back(SomeExp.front().GetValue());
	  OpSymPos.push_back( Word.size() );
	  SomeExp.pop_front();
	  continue;
	}
	for( int j = 0; j < Variables.size(); j++ )
	{
	  if( SomeExp.front().GetValue() == Variables[j].GetName() )
	  {
		Word.push_back(Variables[j].GetValue());
		b = false;
		break;
	  }
	}
	if(b)
	{
	  DefaultOut->Print("Find Erorr!");
	  return false;
	}
  }
  SomeExp.pop_front();
  while( SomeExp.size() )
  {
	bool b = true;
    if( SomeExp.front().GetValue() == "[" )
	{
	  SomeExp.pop_front();
	  std::list<Lexeme> Exp;
	  while( SomeExp.size() && SomeExp.front().GetValue() != "]" )
	  {
		Exp.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  Text += ( Calculate( "int" , Exp , Variables ) );
	  continue;
	}
	if( SomeExp.front().GetValue() == "<" )
	{
	  SomeExp.pop_front();
	  std::list<Lexeme> Exp;
	  while( SomeExp.size() && SomeExp.front().GetValue() != ">" )
      {
		Exp.push_back( SomeExp.front() );
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  Text += ( UnicodeString(char(Calculate( "int" , Exp , Variables ).ToInt())) );
	  continue;
	}
	if( SomeExp.front().GetValue() == "\"" )
	{
	  SomeExp.pop_front();
	  while( SomeExp.size() && SomeExp.front().GetValue() != "\"" )
	  {
		Text += (SomeExp.front().GetValue());
		SomeExp.pop_front();
	  }
	  SomeExp.pop_front();
	  continue;
	}
	for( int j = 0; j < Variables.size(); j++ )
	{
	  if( SomeExp.front().GetValue() == Variables[j].GetName() )
	  {
		Text += Variables[j].GetValue();
		b = false;
		SomeExp.pop_front();
		break;
	  }
	}
	if(b)
	{
	  DebugOut->Print("Find Erorr!");
	  return false;
	}
  }
  if( !Text.Length() )
  {
    DebugOut->Print("[Error] ����� � ������� �����");
	return false;
  }
  i = 0;
  b = true;
  bool star=false;
  int pos = 1;
  int backpos = 1;
  int tmp = 0;
  int ID = Word.size();
  UnicodeString Res;
  Res = "������: \"";
  while( i < Text.Length() && Word.size() )
  {
	while( Word.front() == "\?" && InVect( int(ID - Word.size() + 1) , OpSymPos ) )
	{
	  backpos++;
	  Word.pop_front();
	  Res+="\"-\"";
	}
	while( Word.front() == "*" && InVect( int(ID - Word.size() + 1) , OpSymPos ))
	{
	  star = true;
	  Word.pop_front();
	  Res+="\"<-->\"";
	}
	tmp = FindInText( Word.front() , Text , backpos );
	Res += Word.front();
	if( tmp > 0)
	{
	  if( star )
	  {
		backpos = tmp + Word.front().Length();
		star = false;
	  }
	  else
	  {
		if( tmp == backpos )
		{
		  b = b && true;
		  backpos += tmp + Word.front().Length();
		}
		else
		{
		  b = false;
		}
	  }
	}
	else
	{
	  b = false;
	}
	Word.pop_front();
	i++;

  }
  DefaultOut->Print(Res + "\" � �����: \"" + Text + "\"");
  if( b )
  {
	DefaultOut->Print("����� ��������");
  }
  else
  {
	DefaultOut->Print("����� �� ��������");
  }
  return true;
}

