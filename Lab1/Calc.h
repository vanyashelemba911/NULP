#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <math.h>
#include <windows.h>

struct ExpW
{
  std::vector< std::string > SomeExp;
  std::vector< int > Weights;
};

//------------------------------------------------------------------------------
class Node{
  private:
	Node* F;
  public:
	Node(Node* f);
	std::string Data;
	Node* LeftNode;
	Node* RightNode;
	Node* Father();
	//~Node(); //fix it
};

//------------------------------------------------------------------------------
template <class T>
bool InVect( const T &val , const std::vector<T> &vect );

bool ChInString( const char &c , const std::string &st );

bool IsNumber( const std::string &st );

bool ShowTree( Node* Tree , const int &depth , const int &W , const bool &Left );

std::string Clc( std::vector< std::string > SomeExp );
std::string ClcB( std::vector< std::string > SomeExp );
std::string CalculateTree( Node* Tree );

ExpW CreateExpW( std::vector< std::string > &SomeExp );

Node* AddNodes( const ExpW ExpWei , Node* F );

