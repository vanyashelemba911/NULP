#include "Calc.h"
#include "Declarations.h"
bool ChInString( const char &c , const std::string &st )
{
  for( int i = 0; i < st.size(); i++ )
	if( c == st[i] ) return true;
  return false;
}
//------------------------------------------------------------------------------

bool IsNumber( const std::string &st )
{
  for( int i = 0; i < st.size(); i++ )
	if( !isdigit(st[i]) ) return false;
  return true;
}
//------------------------------------------------------------------------------

template <class T>
bool InVect( const T &val , const std::vector<T> &vect )
{
  for( int i = 0; i < vect.size(); i++ )
  {
	if( val == vect[i] ) return true;
  }
  return false;
}
//------------------------------------------------------------------------------
Node::Node(Node* f) { this->F = f; }

Node* Node::Father() { return this->F; }

//------------------------------------------------------------------------------

ExpW CreateExpW( std::vector< std::string > &SomeExp )
{
  ExpW res;
  res.SomeExp.reserve( SomeExp.size() );
  res.Weights.reserve( SomeExp.size() );
  int KD = 0;
  int MaxPr = 5;
  int W = -1;
  for( int i = 0; i < SomeExp.size(); i++ )
  {
	if( SomeExp[i] == "(" ) KD++;
	if( SomeExp[i] == ")" ) KD--;
  }
  if( KD ) throw 20;

  for( int i = 0 ; i < SomeExp.size(); i++ )
  {
	if( SomeExp[i] == "(" )
	{
	  if( !IsNumber( SomeExp[i+1] ) && SomeExp[i+1] != "-" && SomeExp[i+1] != "(" ) throw 21;
	  KD++;
	  continue;
	}
	if( SomeExp[i] == ")" )
	{
	  if( !IsNumber( SomeExp[i-1] ) && SomeExp[i-1] != ")" ) throw 22;
	  KD--;
	  continue;
	}
	if( i > 0 && i < SomeExp.size() - 1 && SomeExp[i] == "-" && ( !IsNumber( SomeExp[i-1] ) && IsNumber( SomeExp[i+1] )) )
	{
	  res.Weights.push_back( (KD + 1) * MaxPr + 1 );
	  res.SomeExp.push_back( SomeExp[i] );
	  continue;
	}
	W = - 1;
	if( SomeExp[i] == "+" ) W = ( KD * MaxPr + 1 );
	if( SomeExp[i] == "-" ) W = ( KD * MaxPr + 1 );
	if( SomeExp[i] == "*" ) W = ( KD * MaxPr + 3 );
	if( SomeExp[i] == "/" ) W = ( KD * MaxPr + 3 );
	if( SomeExp[i] == "^" ) W = ( KD * MaxPr + 5 );
	res.SomeExp.push_back( SomeExp[i] );
	res.Weights.push_back( W );
  }
  for( int i = 0 ; i < res.SomeExp.size(); i++ )
  {
	std::cout << res.SomeExp[i] << " ";
  }
  std::cout << "\n";
  for( int i = 0 ; i < res.Weights.size(); i++ )
  {
	std::cout << res.Weights[i] << " ";
  }
  std::cout << "\n";
  std::cin.get();
  std::cin.get();
  return res;
}

Node* AddNodes( const ExpW ExpWei , Node* F )
{
  Node* Res = new Node(F);
  if( ExpWei.SomeExp.size() == 3 )
  {
	Res->LeftNode = new Node(Res);
	Res->RightNode = new Node(Res);
	Res->Data = ExpWei.SomeExp[1];
	Res->LeftNode->Data = ExpWei.SomeExp[0];
	Res->RightNode->Data = ExpWei.SomeExp[2];
	return Res;
  }
  if( ExpWei.SomeExp.size() == 2 )
  {
	Res->RightNode = new Node(Res);
	Res->Data = ExpWei.SomeExp[0];
	Res->RightNode->Data = ExpWei.SomeExp[1];
	return Res;
  }
  if( ExpWei.SomeExp.size() == 1 )
  {
	Res->Data = ExpWei.SomeExp[0];
	return Res;
  }
  int pos;
  for( int i = 0; i < ExpWei.Weights.size(); i++ )
	if( ExpWei.Weights[i] != -1 ) { pos = i; break; }
  for( int i = 0; i < ExpWei.Weights.size() ; i++ )
  {
	if( ExpWei.Weights[i] != -1 && ExpWei.Weights[pos] >= ExpWei.Weights[i]  )
	  pos = i;
  }
  ExpW EW;
  EW.SomeExp.reserve( pos );
  EW.Weights.reserve( pos );
  ExpW EW1;
  EW1.SomeExp.reserve( ExpWei.SomeExp.size() - pos );
  EW1.Weights.reserve( ExpWei.SomeExp.size() - pos );

  for( int i = 0; i < pos && i < ExpWei.SomeExp.size(); i++ )
  {
	EW.SomeExp.push_back( ExpWei.SomeExp[i] );
	EW.Weights.push_back( ExpWei.Weights[i] );
  }
  Res->Data = ExpWei.SomeExp[pos];
  Res->LeftNode = AddNodes( EW , Res );
  for( int i = pos+1; i < ExpWei.SomeExp.size(); i++ )
  {
	EW1.SomeExp.push_back( ExpWei.SomeExp[i] );
	EW1.Weights.push_back( ExpWei.Weights[i] );
  }
  Res->RightNode = AddNodes( EW1 , Res );
  return Res;
}
bool ShowTree( const std::vector<std::string> &SomeExp )
{
  UnicodeString st;
  st = "calc.exe ";
  for( int i = 0; i < SomeExp.size(); i++ )
  {
	st += SomeExp[i].c_str();
	st += " ";
  }
  system( AnsiString(st).c_str() );
  return true;
}
//------------------------------------------------------------------------------

std::string Clc( std::vector<std::string> SomeExp )
{
  Node *Tree;
  Tree = AddNodes( CreateExpW(SomeExp) , 0 );
  if( StartConsole ) ShowTree( SomeExp );
  return CalculateTree( Tree );
}

std::string ClcB( std::vector<std::string> SomeExp )
{
  Node *Tree;
  Tree = AddNodes( CreateExpW(SomeExp) , 0 );
  if( StartConsole ) ShowTree( SomeExp );
  if( CalculateTree( Tree ) != "0" )
	return "true";
  return "false";
}


//------------------------------------------------------------------------------
std::string CalculateTree( Node* Tree )
{
  char bufer[255];
  if( IsNumber( Tree->Data ) ) return Tree->Data;
  if( Tree->Data == "+" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) + std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "-" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) - std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "*" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) * std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "/" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) / std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "^" ) return std::itoa( pow( double(std::atoi( CalculateTree( Tree->LeftNode ).c_str() )) , std::atoi( CalculateTree( Tree->RightNode ).c_str() ) ) , bufer , 10 );

  if( Tree->Data == ">" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) > std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "<" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) < std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == ">=" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) >= std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "<=" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) <= std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  if( Tree->Data == "==" ) return std::itoa( std::atoi( CalculateTree( Tree->LeftNode ).c_str() ) == std::atoi( CalculateTree( Tree->RightNode ).c_str() ) , bufer , 10 );
  return "0";
}
