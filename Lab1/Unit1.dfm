object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 1011
  ClientWidth = 1914
  Color = clBtnFace
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 20
    Top = 144
    Width = 726
    Height = 674
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Consolas'
    Font.Style = []
    Lines.Strings = (
      'begin'
      #9'//'#1050#1086#1084#1077#1085#1090#1072#1088' '#8470'1'
      #9'/* '#1050#1086#1084#1077#1085#1090#1072#1088' '#8470'2 */'
      #9'string SomeText; //'#1050#1086#1084#1077#1085#1090#1072#1088' '#8470'3'
      #9'string SomeText1= "regerh 20g"; /* '#1050#1086#1084#1077#1085#1090#1072#1088' '#8470'4 */'
      #9'SomeText = "iohiqbhk kjkj sdaojsjflsanf";'
      #9'int pos;'
      #9'pos = 10;'
      #9'int i = pos + 2 * 5;'
      #9'while( i < 10 )'
      #9'{'
      #9'  if( i < 5 ) '
      #9'  {'
      #9'    pos = pos + i * 2;'
      #9'  }'
      #9'  else'
      #9'  {'
      #9'    pos = pos + i;'
      #9'  }'
      #9'  i = i + 1;'#9'  '
      #9'}'
      #9'int sym = 10;'
      #9'print "pos = " pos ;'
      #9'find *"kj"?"j" in SomeText;'
      #9'thread th1;'
      
        #9'thread th2 << find *[2*sym]"g" in SomeText1; //'#1079#1085#1072#1081#1090#1080' [2*sym]"g' +
        '" ===> 20g'
      #9'th1 << find *"kj"*"j" in SomeText;'
      'end')
    ParentFont = False
    PopupMenu = PopupMenuEditor
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object Memo2: TMemo
    Left = 752
    Top = 144
    Width = 665
    Height = 674
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
  end
  object Memo3: TMemo
    Left = 20
    Top = 824
    Width = 1397
    Height = 152
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 2
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 976
    Width = 1914
    Height = 35
    Panels = <>
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1914
    Height = 138
    Align = alTop
    TabOrder = 4
    object Button1: TButton
      Left = 50
      Top = 21
      Width = 75
      Height = 25
      Caption = 'Start'
      TabOrder = 0
      OnClick = Button1Click
    end
    object GroupBox1: TGroupBox
      Left = 312
      Top = 0
      Width = 337
      Height = 121
      Caption = #1042#1080#1074#1110#1076
      TabOrder = 1
      object CheckBox1: TCheckBox
        Left = 16
        Top = 25
        Width = 97
        Height = 17
        Caption = #1050#1086#1085#1089#1086#1083#1100
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CheckBox1Click
      end
      object CheckBox2: TCheckBox
        Left = 16
        Top = 60
        Width = 97
        Height = 17
        Caption = #1044#1077#1073#1072#1075
        Checked = True
        State = cbChecked
        TabOrder = 1
        OnClick = CheckBox2Click
      end
      object CheckBox3: TCheckBox
        Left = 16
        Top = 95
        Width = 97
        Height = 17
        Caption = #1042#1080#1074#1110#1076' '#1079' '#1087#1088#1086#1075#1088#1072#1084#1080
        Checked = True
        State = cbChecked
        TabOrder = 2
        OnClick = CheckBox3Click
      end
      object CheckBox4: TCheckBox
        Left = 192
        Top = 25
        Width = 97
        Height = 17
        Caption = #1044#1077#1088#1077#1074#1086' '#1074#1080#1088#1072#1079#1091
        TabOrder = 3
        OnClick = CheckBox4Click
      end
    end
    object Button2: TButton
      Left = 50
      Top = 76
      Width = 75
      Height = 25
      Caption = 'Stop'
      TabOrder = 2
      OnClick = Button2Click
    end
  end
  object Panel2: TPanel
    Left = 1432
    Top = 138
    Width = 482
    Height = 838
    Align = alRight
    TabOrder = 5
    object Memo4: TMemo
      Left = 16
      Top = 6
      Width = 457
      Height = 826
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      ScrollBars = ssVertical
      TabOrder = 0
    end
  end
  object MainMenu1: TMainMenu
    Left = 856
    Top = 16
    object N11: TMenuItem
      Caption = 'File'
      object Open1: TMenuItem
        Caption = 'Open'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = 'Save'
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save As'
        OnClick = SaveAs1Click
      end
    end
    object About1: TMenuItem
      Caption = 'About'
    end
  end
  object PopupMenuEditor: TPopupMenu
    Left = 784
    Top = 64
    object Clear1: TMenuItem
      Caption = 'Clear'
      OnClick = Clear1Click
    end
  end
  object OpenDialog1: TOpenDialog
    Left = 984
    Top = 24
  end
  object SaveDialog1: TSaveDialog
    Left = 928
    Top = 88
  end
end
