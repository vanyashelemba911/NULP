//---------------------------------------------------------------------------

#include <vcl.h>
#include <windows.h>
#pragma hdrstop

#include "Unit1.h"

#include "Interface.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;

StandartOut* ConsoleOut = new StandartOut;
StandartOut* DebugOut = new StandartOut;
StandartOut* DefaultOut = new StandartOut;

bool StartConsole = false;

HANDLE *MainThread;
DWORD _stdcall MainFoo(void *);

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Clear1Click(TObject *Sender)
{
  Form1->Memo1->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
  ConsoleOut->SetOutput( this->Memo4 );
  DebugOut->SetOutput( this->Memo3 );
  ConsoleOut->SetArrow(" ");
  DefaultOut->SetOutput( this->Memo2 );
  DefaultOut->SetArrow("");
  Form1->DoubleBuffered = true;
  InitializeCriticalSection(&cs);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  if( MainThread )
  {
	WaitForSingleObject(*MainThread , INFINITE );
	CloseHandle(*MainThread);
	delete MainThread;
  }
  MainThread = new HANDLE;
  *MainThread = CreateThread(0,0,MainFoo,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Open1Click(TObject *Sender)
{
  Form1->OpenDialog1->Execute();
  if( Form1->OpenDialog1->FileName != "" )
	Form1->Memo1->Lines->LoadFromFile( Form1->OpenDialog1->FileName );
}
//---------------------------------------------------------------------------
DWORD _stdcall MainFoo(void *)
{
  Form1->Memo2->Clear();
  Form1->Memo3->Clear();
  Form1->Memo4->Clear();
  Interpretator *Interp = new Interpretator;
  DebugOut->Print("�������");
	DebugOut->Print("��������� �����");
	if(Interp->Parser(Form1->Memo1->Text))
	  DebugOut->Print("����� �������.");
	else
	  {
		DebugOut->Print("[Error] ������� ������� ��� �������i ������");
		delete Interp;
		return 0;
	  }
	ConsoleOut->Print(Interp->ShowTokens());
	ConsoleOut->Print("");ConsoleOut->Print("");ConsoleOut->Print("");
	ConsoleOut->Print(Interp->ShowLexemes());
	ConsoleOut->Print("");ConsoleOut->Print("");ConsoleOut->Print("");
	DebugOut->Print("��������� ��������");
	if ( Interp->Start() )
	{
        DebugOut->Print("");
	}
	else
	{
		DebugOut->Print("[Error] ������� ������� ��� ��������� ��������");
		delete Interp;
		return 0;
	}
  DebugOut->Print("��������� ���������!");

  delete Interp;
  return 0;
}

void __fastcall TForm1::CheckBox1Click(TObject *Sender)
{
  if( !this->CheckBox1->Checked )
  {
	ConsoleOut->Mute();
  }
  else
  {
	ConsoleOut->UnMute();
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox2Click(TObject *Sender)
{
  if( !this->CheckBox2->Checked )
  {
	DebugOut->Mute();
  }
  else
  {
	DebugOut->UnMute();
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox3Click(TObject *Sender)
{
  if( !this->CheckBox3->Checked )
  {
	DefaultOut->Mute();
  }
  else
  {
	DefaultOut->UnMute();
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::CheckBox4Click(TObject *Sender)
{
  if( !this->CheckBox4->Checked )
  {
	StartConsole = false;
  }
  else
  {
	StartConsole = true;
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SaveAs1Click(TObject *Sender)
{
  Form1->SaveDialog1->Execute();
  if( Form1->SaveDialog1->FileName != "" )
	Form1->Memo1->Lines->SaveToFile( Form1->SaveDialog1->FileName );
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Save1Click(TObject *Sender)
{
  Form1->Memo1->Lines->SaveToFile( "CODE.txt" );
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
  if( MainThread )
  {
	TerminateThread(*MainThread,0);
	CloseHandle(*MainThread);
	delete MainThread;
    DebugOut->Print("<<<<��������� �������� ��������� ����������!>>>>");
  }
}
//---------------------------------------------------------------------------

