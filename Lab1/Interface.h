#pragma once
#include "Declarations.h"
#include "Calc.h"
class Interpretator
{
  private:
	std::vector<Token>  TokenArr;                         //  ������ �������
	std::vector<Lexeme> LexemeArr;                        //  ������ ������
	std::vector< UnicodeString > VarNames;                //  ��� ����������� �����, �� �������� �� ���
	std::vector< UnicodeString > ThreadNames;             //  ��������� - ������

	bool SecondCall;
	//=================
	bool SetTokens(UnicodeString Code);                   //�������� ������ � ����
	bool SetLexemes( );                                   //�������� ������� � �������
	bool FillTables( );
	bool ClrMem( );

	bool Exec( std::vector<Lexeme> &LexemeArr , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads );
	bool While( std::list<Lexeme> &SomeExp , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads );
	bool If( std::list<Lexeme> &SomeExp , std::vector< SomeVar > &Variables , std::vector< SomeThread > &Threads );
	bool PrintSmth( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );

  public:
	Interpretator( );
	Interpretator( const UnicodeString &Code );

	bool Parser( const UnicodeString &Code );
	bool Start();

	UnicodeString ShowTokens( );
	UnicodeString ShowLexemes( );
};

