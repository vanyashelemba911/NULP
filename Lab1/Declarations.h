#pragma once
#include <vcl.h>
#include <list>
#include <vector>
#include <windows.h>
//==============================================================================

enum TType { T_KEY_WORD_ , T_DATA_TYPE_ , T_SOME_SYM_ , T_VOID_}; // T_VOID_ - ��������������� �� �������� �������� ��� ��������� ��'���� � ����������� ��� ���������
enum LType { L_VOID_ , KEY_WORD , DATA_TYPE , VARIABLE , SOME_SYM , SOME_OP , SEPSYM , VALUE , THREAD_IDENT }; //SOME_OP - ����� ��������. SOME_SYM - ������ ������. L_VOID - �������� ��������

const UnicodeString Alphabet        = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789_"; //��������������� ��� �������� �� ��������� ��'� �����
const UnicodeString KeyWords 		= "begin end for while if else thread find in const print"; //��������������� ��� �������� �� � ����� �������� ������
const UnicodeString DataTypes		= "int char string bool"; //��������������� ��� �������� �� � ����� ����� �����
const UnicodeString Symbols		 	= ";\"&!=<>()[]{}+-*\/|\\\?\':.,"; //��������������� ��� ������� �� ������

const int NO_OF_CHARS = 255; //��������������� � �������� ������ ����-����
extern int id;
CRITICAL_SECTION cs;

extern bool StartConsole;

//==============================================================================
class StandartOut                                     	  //��� ������������
{
  private:
	TMemo* Out;
	UnicodeString OldData;
	UnicodeString Arrow;
	bool Muted;
  public:
    StandartOut()
	{
	  Arrow = "|-> ";
	  OldData = "";
	  Out = 0;
	  Muted = false;
	}
	bool Print( const UnicodeString &Message );
	bool Print( const UnicodeString &Message , const unsigned int &l);
	bool Mute();
	bool UnMute();
	bool GotoXY( const int &x , const int &y);
	bool SetOutput( TMemo* Otput );
	bool SetArrow( const UnicodeString &Arrow );
	bool ShowOld();
};
//------------------------------------------------------------------------------
extern StandartOut* ConsoleOut;
extern StandartOut* DebugOut;
extern StandartOut* DefaultOut;
//------------------------------------------------------------------------------
class Texts
{
  public:
	UnicodeString Text;
	std::vector<UnicodeString> Words;
	Texts();
	Texts( const UnicodeString &T , const std::vector<UnicodeString> &W );
};
//------------------------------------------------------------------------------

class Token
{
  private:
	UnicodeString Value;
	TType TokenType;
  public:
	Token();
	Token( const UnicodeString &Val , const TType &T );
	UnicodeString GetValue();
	TType GetTType();
};
//------------------------------------------------------------------------------

class Lexeme
{
  private:
	UnicodeString Value;
	LType LexemeType;
  public:
	Lexeme();
	Lexeme( const UnicodeString &Val , const LType &L );
	UnicodeString GetValue();
	LType GetLType();
};
//------------------------------------------------------------------------------

class SomeVar
{
  private:
	UnicodeString Type;
	UnicodeString Name;
	UnicodeString Value;
	bool CanChange;
  public:
	SomeVar();
	SomeVar( const UnicodeString &Type , const UnicodeString &Name , const UnicodeString &Value , const bool &CanChange );

	bool Initialize( const UnicodeString &Type , const UnicodeString &Name , const UnicodeString &Value , const bool &CanChange );
	bool ChangeValue( const UnicodeString &NewValue );

	const UnicodeString GetName();
	const UnicodeString GetType();
	const UnicodeString GetValue();
};

//------------------------------------------------------------------------------

class SomeThread
{
  private:
	UnicodeString Name;
	HANDLE hThread;
	int ID;
  public:
	SomeThread( const UnicodeString &Name );
	bool Start( std::list<Lexeme> SomeExp ,  std::vector< SomeVar > Variables );
	UnicodeString GetName();
};
//==============================================================================
struct sData{ std::list<Lexeme> SomeExp ;  std::vector< SomeVar > Variables; };
