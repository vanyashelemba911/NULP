#pragma once
#include "Declarations.h"

//==============================================================================
bool CheckVarName(const UnicodeString &ch );   // �������������� ��� �������� ����������� ����� �����
bool IsKeyWord	( const UnicodeString &ch );   // �������������� ��� �������� �� ��������� ����� � �������� ������
bool IsSomeSym 	( const UnicodeString &ch );   // �������������� ��� �������� �� ��������� ����� � ������� ��������
bool IsDataType	( const UnicodeString &ch );   // �������������� ��� �������� �� ��������� ����� � ����� �����
bool isdigit	( const wchar_t &c );          // �������������� � ����� ��������, ��� �������� �� ������ � ������
bool IsSomeOp   ( const UnicodeString &ch );   //

int SearchIn ( const UnicodeString &text , const UnicodeString &word ); // �������������� ��� ������ ����� � ����� �������� ( �������� ����-���� )

UnicodeString TTypeToString( const TType &SomeType ); // �������������� ��� ����������� ���� ������ � ������ UnicodeString
UnicodeString LTypeToString( const LType &SomeType ); // �������������� ��� ����������� ���� ������� � ������ UnicodeString

TType GetTType( const UnicodeString &TypeSt ); // �������������� ������������� ���� ������ �� ����� ( ���������: �������� - "begin", ��������� T_KEY_WORD_)

DWORD _stdcall FindText( void *p );
bool Find( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );

UnicodeString Calculate( const UnicodeString &ResType , std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );
UnicodeString CalculateInt   ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );
UnicodeString CalculateBool  ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );
UnicodeString CalculateChar  ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );
UnicodeString CalculateFloat ( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );
UnicodeString CalculateString( std::list<Lexeme> &SomeExp ,  std::vector< SomeVar > &Variables );

