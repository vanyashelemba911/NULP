#include "Declarations.h"
#include "OtherFunctions.h"
#include "Calc.h"

int id;

//==================================Texts=======================================
Texts::Texts()
{
  this->Text = "";
}

Texts::Texts( const UnicodeString &T , const std::vector<UnicodeString> &W )
{
  this->Text = T;
  this->Words.reserve( W.size() );
  for( int i = 0; i < W.size(); i++ ) this->Words.push_back( W[i] );
}

//==================================Token=======================================
Token::Token()
{
  this->Value = "";
  this->TokenType = T_VOID_;
}

Token::Token( const UnicodeString &Val , const TType &T )
{
  this->Value = Val;
  this->TokenType = T;
}

UnicodeString Token::GetValue()
{
  return this->Value;
}

TType Token::GetTType()
{
  return this->TokenType;
}

//=================================Lexeme=======================================
Lexeme::Lexeme()
{
  this->Value = "";
  this->LexemeType = L_VOID_;
}

Lexeme::Lexeme( const UnicodeString &Val , const LType &L )
{
  this->Value = Val;
  this->LexemeType = L;
}

UnicodeString Lexeme::GetValue()
{
  return this->Value;
}

LType Lexeme::GetLType()
{
  return this->LexemeType;
}

//==================================SomeVar=====================================
SomeVar::SomeVar()
{
  this->Type = "";
  this->Name = "";
  this->Value = "";
  CanChange = true;
}

SomeVar::SomeVar( const UnicodeString &Type , const UnicodeString &Name , const UnicodeString &Value , const bool &CanChange )
{
  this->Type = Type;
  this->Name = Name;
  this->Value = Value;
  this->CanChange = CanChange;
}

bool SomeVar::Initialize( const UnicodeString &NewType , const UnicodeString &Name , const UnicodeString &NewValue , const bool &CanChange )
{
  this->Type = NewType;
  this->Name = Name;
  this->CanChange = CanChange;
  if (NewType == "int")
	{
	  if( IsNumber(AnsiString(NewValue).c_str()) )
		this->Value = NewValue;
	  else
		return false;
	}
	if (NewType == "char")
	{
	 if( NewValue.Length() <= 1 )
		this->Value = NewValue;
	  else
		return false;
	}
	if (NewType == "string")
	{
	 this->Value = NewValue;
	}
	if (NewType == "float")
	{
	  this->Value = NewValue;
	}
	if (NewType == "bool")
	{
	  if( NewValue == "false" || NewValue == "true" )
		this->Value = NewValue;
	  else
		return false;
	}
  return true;
}

bool SomeVar::ChangeValue( const UnicodeString &NewValue )
{
  if( !this->CanChange ) return false;
  if (Type == "int")
	{
	  if( IsNumber(AnsiString(NewValue).c_str()) )
		this->Value = NewValue;
	  else
		return false;
	}
	if (Type == "char")
	{
	 if( NewValue.Length() <= 1 )
		this->Value = NewValue;
	  else
		return false;
	}
	if (Type == "string")
	{
	 this->Value = NewValue;
	}
	if (Type == "float")
	{
	  this->Value = NewValue;
	}
	if (Type == "bool")
	{
	  if( NewValue == "false" || NewValue == "true" )
		this->Value = NewValue;
	  else
		return false;
	}
  return true;
}

const UnicodeString SomeVar::GetName()
{
  return this->Name;
}

const UnicodeString SomeVar::GetType()
{
  return this->Type;
}

const UnicodeString SomeVar::GetValue()
{
  return this->Value;
}

//==================================Thread======================================
SomeThread::SomeThread( const UnicodeString &Name )
{
  ID = id++;
  this->Name = Name;
}

bool SomeThread::Start( std::list<Lexeme> SomeExp ,  std::vector< SomeVar > Variables )
{
  DefaultOut->SetArrow("Thread#" + UnicodeString(ID) + "-->> ");
  sData SearchData;
  SearchData.SomeExp = SomeExp;
  SearchData.Variables = Variables;
  this->hThread = CreateThread(NULL,0,FindText,&SearchData,0,0);
  WaitForSingleObject(this->hThread , INFINITE);
  CloseHandle(this->hThread);
  DefaultOut->SetArrow("");
  return true;
}
UnicodeString SomeThread::GetName()
{
  return this->Name;
}

//==============================================================================

