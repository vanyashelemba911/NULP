//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
#include "GlobalDeclarations.h"
#include "Functions.h"
#include "Engine.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
GameManager *Manager;
ActionLog *LOG;
GameSettings *GS = new GameSettings(180,70,0,"Ivan",18,0,NORMAL,0);
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn2Click(TObject *Sender)
{
  if(StartButton(GS,Manager,Form1))
  {
	LOG->Write("All good! StartButton(GS,Manager,Form1)");
  }
  else
  {
	LOG->Write("Fail to start game!");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn4Click(TObject *Sender)
{
  if(ExitButton(Manager))
  {
	LOG->Write("All good! ExitButton()");
	this->Close();
  }
  else
  {
	LOG->Write("Fail to exit game!");
	LOG->SaveToFile("Crashreport.txt");
	this->Close();
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1Click(TObject *Sender)
{
  if(OptionsButton())
  {
	Form2->Show();
  }
  else
  {
	LOG->Write("Fail to open options!");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn3Click(TObject *Sender)
{
  Form3->Show();
  LogButton(Form3->Memo1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormShow(TObject *Sender)
{
  this->DoubleBuffered = true;
  LOG = new ActionLog;
  LOG->Write("����������� ������ �������...");
  GraphEngine* GEngine = GraphEngine::getInstance(this);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn2MouseEnter(TObject *Sender)
{
  int size = 10;
  this->BitBtn2->Width -= size;
  this->BitBtn2->Height -= size;
  this->BitBtn2->Top += size/2;
  this->BitBtn2->Left += size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1MouseEnter(TObject *Sender)
{
  int size = 10;
  this->BitBtn1->Width -= size;
  this->BitBtn1->Height -= size;
  this->BitBtn1->Top += size/2;
  this->BitBtn1->Left += size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn2MouseLeave(TObject *Sender)
{
  int size = 10;
  this->BitBtn2->Width += size;
  this->BitBtn2->Height += size;
  this->BitBtn2->Top -= size/2;
  this->BitBtn2->Left -= size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn1MouseLeave(TObject *Sender)
{
  int size = 10;
  this->BitBtn1->Width += size;
  this->BitBtn1->Height += size;
  this->BitBtn1->Top -= size/2;
  this->BitBtn1->Left -= size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn4MouseEnter(TObject *Sender)
{
  int size = 10;
  this->BitBtn4->Width -= size;
  this->BitBtn4->Height -= size;
  this->BitBtn4->Top += size/2;
  this->BitBtn4->Left += size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn4MouseLeave(TObject *Sender)
{
  int size = 10;
  this->BitBtn4->Width += size;
  this->BitBtn4->Height += size;
  this->BitBtn4->Top -= size/2;
  this->BitBtn4->Left -= size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn3MouseEnter(TObject *Sender)
{
  int size = 10;
  this->BitBtn3->Width -= size;
  this->BitBtn3->Height -= size;
  this->BitBtn3->Top += size/2;
  this->BitBtn3->Left += size/2;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BitBtn3MouseLeave(TObject *Sender)
{
  int size = 10;
  this->BitBtn3->Width += size;
  this->BitBtn3->Height += size;
  this->BitBtn3->Top -= size/2;
  this->BitBtn3->Left -= size/2;
}
//---------------------------------------------------------------------------

