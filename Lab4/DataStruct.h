#pragma once
#include "GlobalDeclarations.h"
#include <vcl.h>
enum Difficulty    { POTATO , EASY , NORMAL , HARD , GODMODE , WTF_MODE };
enum CardType      { JUSTCARD , BONUSCARD , WTFCARD , JOKER };
enum CardTexture_T { T_AppleCard , T_BananaCard , T_BonusScore , T_FreezeCard , T_GrapeCard , T_HummerCard , T_LemonCard , T_MinusScore , T_OrangeCard , T_PeachCard , T_PearCard , T_PineappleCard , T_PlumCard , T_ShieldCard , T_XrayCard };
enum SomeEvent     { GAMESTART , GAMEFREZE , GAMESTOP , SELECTCARD , SELECTEFFECT , DRAWTEXTURES , HIDECARD , DELETECARD};

struct NetFooData
{
  void* BufferIn;
  CRITICAL_SECTION cs;
  NetFooData(CRITICAL_SECTION &C , void* B) { BufferIn = B; cs = C; }
};
class GameSettings
{
  private:
	int Health;
	int Time;
	int Score;
	int GameStyle;
	UnicodeString UserName;
	int CardsNum;
	int BonusCardsNum;
	Difficulty GameDiff;
  public:
	GameSettings(const int& Health, const int& Time,const int& Score, const UnicodeString& UserName,
	const int& CardsNum, const int& BonusCardsNum, const Difficulty& GameDiff , const int& Style);
	bool Reinitialise(const int& Health, const int& Time,const int& Score, const UnicodeString& UserName,
	const int& CardsNum, const int& BonusCardsNum, const Difficulty& GameDiff , const int& Style);
	GameSettings();
	int GetHealth();
	int GetTime();
	int GetScore();
	int GetStyle();
	int GetCardsNum();
	int GetBonusCardsNum();
	Difficulty GetGameDiff();
	UnicodeString GetUserName();
};

class User
{
  private:
	int ID;
	UnicodeString Name;
	int Score;
	UnicodeString Avatar;
  public:
	User(int ID,UnicodeString Name,int Score,UnicodeString Avatar);
	int GetID();
	UnicodeString GetName();
	int GetScore();
	UnicodeString GetAvatar();
};

struct GEngineData
{
  int Style;
  int NumberOfCards;
  CardTexture_T *CardTextures;
};

class SomeEffect
{
  private:
	char EffectID;
  public:
	SomeEffect();
	bool DoSmth();
};

class NetData
{
  private:
	int a;
  public:
	NetData(int a);
	int geta();
};

