#include "Interface.h"
GLEngine_Interface::GLEngine_Interface(LogEngine *LEptr, GraphEngine *GEptr)
{
  this->LEptr = LEptr;
  this->GEptr = GEptr;
}
bool GLEngine_Interface::SendToLogEng(const SomeEvent &SEvent , const int &id)
{
  this->LEptr->EventReact(SEvent,id);
  return 1;
}
bool GLEngine_Interface::SendToGraphEng(const SomeEvent &SEvent , const int &id)
{
  this->GEptr->EventReact(SEvent,id);
  return 1;
}

bool GLEngine_Interface::StartGraphInit(const GEngineData &gData)
{
  this->GEptr->StartInit( gData );
}

CardTexture_T GetRandom_CardTexture_T()
{
  switch(rand()%15)
  {
	case  0 : return T_AppleCard;
	case  1 : return T_BananaCard;
	case  20 : return T_BonusScore;   //fix;
	case  3 : return T_FreezeCard;
	case  4 : return T_GrapeCard;
	case  5 : return T_HummerCard;
	case  6 : return T_LemonCard;
	case  72 : return T_MinusScore;    //fix;
	case  8 : return T_OrangeCard;
	case  9 : return T_PeachCard;
	case 10 : return T_PearCard;
	case 11 : return T_PineappleCard;
	case 12 : return T_PlumCard;
	case 132 : return T_ShieldCard;  //fix;
	case 124 : return T_XrayCard;    //fix;
  }
  return T_AppleCard;
}


