#include "Gamemanager.h"

GameManager::GameManager()
{
  this->LEngine = 0;
  this->GEngine = 0;
  this->NEngine = 0;
  this->UI_Thread = 0;
  this->GL_Thread = 0;
  this->NET_THREAD = 0;
}

bool GameManager::Initialize( GameSettings *Settings , TForm* Owner )
{
  this->Owner = Owner;
  this->Settings = Settings;
}

bool GameManager::Event( const SomeEvent &sEvent )
{
  switch(sEvent)
  {
	GAMESTART   : return Start(); break;
	GAMEFREZE   : return Freze(); break;
	GAMESTOP    : this->LEngine->EventReact(GAMESTOP,0); return true; break;
  }
  return false;
}

bool GameManager::Start()
{
  LOG->Write("������� ���...");

  LOG->Write("����������� ������ �����...");
  this->LEngine = LogEngine::getInstance();

  LOG->Write("����������� ������ �������...");
  this->GEngine = GraphEngine::getInstance(this->Owner);

  LOG->Write("����������� ������ �����...");
  this->NEngine = NetEngine::getInstance();

  LOG->Write("����������� ��������� ������� �� �����...");
  GLEngine_Interface* GLInterface = new GLEngine_Interface( this->LEngine , this->GEngine );

  LOG->Write("����������� ��������� ���������...");
  NLEngine_Interface* NLInterface = new NLEngine_Interface( this->LEngine , this->NEngine );

  LOG->Write("������...");
  this->LEngine->Start( *this->Settings , GLInterface , NLInterface );
  this->GEngine->Start( GLInterface );

  return true;
}
bool GameManager::Freze()
{
  LOG->Write("Game paused");

  return false;
}
bool GameManager::Stop()
{
  LOG->Write("Stoping game");
  //this->LEngine->EventReact(GAMESTOP,0);
  return false;
}
bool GameManager::SelectCard()
{
  LOG->Write("User select card");

  return false;
}
bool GameManager::SelectEffect()
{
  LOG->Write("User Select Effect");

  return false;
}


