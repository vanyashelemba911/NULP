#pragma once
#include <vcl.h>
#include <string>
const unsigned long BUFER_SIZE = 64;
const unsigned long USERNAME_SIZE = 24;
const unsigned long SOMEDATA_SIZE = 10;
const unsigned long MESSAGE_SIZE = BUFER_SIZE - USERNAME_SIZE;
wchar_t PipeNameForServer[] 			= L"\\\\.\\pipe\\D\\$SomePipe1$";
wchar_t PipeNameForUser[] 				= L"\\\\Desktop-1opn2da\\pipe\\D\\$SomePipe1$";
wchar_t PipeNameForServer2[] 			= L"\\\\.\\pipe\\D\\$SomePipe2$";
wchar_t PipeNameForUser2[] 				= L"\\\\Desktop-1opn2da\\pipe\\D\\$SomePipe2$";
const unsigned long STD_WAIT_TIME = 10000;

struct STD_MESSAGE
{
  char UNAME[USERNAME_SIZE];
  char TIME[SOMEDATA_SIZE];
  char SCORE[SOMEDATA_SIZE];
  char CARDS[SOMEDATA_SIZE];
};
struct CONTROL_STD_MESSAGE
{
  int CommandID;
};

STD_MESSAGE CreateMessage(std::string Name, int Time, int Score, int CardsNum);
void SetPipeName( UnicodeString st, wchar_t Name[] );
