#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"
#include <vcl.h>


class ImagesFactory{
  private:
	static TPicture* Card_Back;
	static TPicture* AppleCard;
	static TPicture* BananaCard;
	static TPicture* BonusScore;
	static TPicture* FreezeCard;
	static TPicture* GrapeCard;
	static TPicture* HummerCard;
	static TPicture* LemonCard;
	static TPicture* MinusScore;
	static TPicture* OrangeCard;
	static TPicture* PeachCard;
	static TPicture* PearCard;
	static TPicture* PineappleCard;
	static TPicture* PlumCard;
	static TPicture* ShieldCard;
	static TPicture* XrayCard;
	ImagesFactory();
  public:
	static TPicture* Get_Card_Back(const UnicodeString &st);
	static TPicture* Get_AppleCard(const UnicodeString &st);
	static TPicture* Get_BananaCard(const UnicodeString &st);
	static TPicture* Get_BonusScore(const UnicodeString &st);
	static TPicture* Get_FreezeCard(const UnicodeString &st);
	static TPicture* Get_GrapeCard(const UnicodeString &st);
	static TPicture* Get_HummerCard(const UnicodeString &st);
	static TPicture* Get_LemonCard(const UnicodeString &st);
	static TPicture* Get_MinusScore(const UnicodeString &st);
	static TPicture* Get_OrangeCard(const UnicodeString &st);
	static TPicture* Get_PeachCard(const UnicodeString &st);
	static TPicture* Get_PearCard(const UnicodeString &st);
	static TPicture* Get_PineappleCard(const UnicodeString &st);
	static TPicture* Get_PlumCard(const UnicodeString &st);
	static TPicture* Get_ShieldCard(const UnicodeString &st);
	static TPicture* Get_XrayCard(const UnicodeString &st);
};

class SomeTextures
{
  public:

	virtual bool Draw( TForm* Owner , const GEngineData &gData ) = 0;
	virtual bool Show( const int &id , CardTexture_T CT ) = 0;
	virtual bool Hide( const int &id ) = 0;
	virtual bool Delete( const int &id ) = 0;
	virtual bool CloseCard( const int &id ) = 0;
	virtual bool Cleanup() = 0;
};

class GraphEngine;
class MyImageTexture : public TImage
{
  public:
	__fastcall MyImageTexture(TComponent* AOwner) : TImage(AOwner){};
	int id;
	GraphEngine *OwnerEngine;
	void __fastcall MyClick(TObject* Sender);
	void __fastcall MyOnMouseEnter(TObject* Sender);
	void __fastcall MyOnMouseLeave(TObject* Sender);
};

class DefTextures : public SomeTextures
{
	MyImageTexture **CardTexture;
	TImage *Effects;
	int NumOfCards;
  public:
	bool Draw( TForm* Owner , const GEngineData &gData );
	bool Show( const int &id , CardTexture_T CT );
	bool Hide( const int &id );
	bool Delete( const int &id );
	bool CloseCard( const int &id );
	bool Cleanup();
	~DefTextures();
};

class RedTextures : public SomeTextures
{
	MyImageTexture **CardTexture;
	TImage *Effects;
	int NumOfCards;
  public:
	bool Draw( TForm* Owner , const GEngineData &gData );
	bool Show( const int &id , CardTexture_T CT );
	bool Hide( const int &id );
	bool Delete( const int &id );
	bool CloseCard( const int &id );
	bool Cleanup();
	~RedTextures();
};

class TextureFactory
{
  public:
	virtual SomeTextures* CreateTextures() = 0;
};

class RedTextureFactory : public TextureFactory
{
  public:
  SomeTextures* CreateTextures()
  {
	return new RedTextures;
  }
};

class DefTextureFactory : public TextureFactory
{
  public:
  SomeTextures* CreateTextures()
  {
	return new DefTextures;
  }
};
