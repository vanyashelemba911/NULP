#include "Entity.h"
Card* BonusCard::Clone()
{
  BonusCard *Ret = new BonusCard;
  Ret->Initialize( this->Number , this->CType , this->CardTextureType );
  return Ret;
}
bool BonusCard::Initialize( const int &n, const CardType &CType , const CardTexture_T CardTextureType )
{
  this->Number = n;
  this->CType = CType;
  this->CardTextureType = CardTextureType;
  this->ScoreBoost = n * 1000;
  return true;
}
int Card::GetScoreBoost()
{
  return this->ScoreBoost;
}
CardType Card::GetCardType()
{
  return this->CType;
}
CardTexture_T Card::GetCardTextureType()
{

  return this->CardTextureType;
}
CardTexture_T JustCard::GetCardTextureType()
{
  return this->CardTextureType;
}
bool JustCard::Initialize( const int &n, const CardType &CType , const CardTexture_T CardTextureType )
{
  this->Number = n;
  this->CType = CType;
  this->CardTextureType = CardTextureType;
  this->ScoreBoost = n * 100;
  this->Damage = (n%3+1) * 10;
  return true;
}
int JustCard::GetDamage()
{
  return this->Damage;
}
JustCard* JustCard::Clone()
{
  JustCard *Ret = new JustCard;
  Ret->Initialize( this->Number , this->CType , this->CardTextureType );
  return Ret;
}
