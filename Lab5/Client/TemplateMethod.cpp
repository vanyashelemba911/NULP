#include "TemplateMethod.h"
#include <stdlib.h>
CardDeck CardFactory::GetCardDeck(const int &size)
{
  CardDeck* CD = new CardDeck(State(size,CreateCards(size)));
  return *CD;
}

Card* JustCardFactory::CreateCards(const int &size)
{
  Card* Cards = new Card[size];
  for(int i = 0; i < size; i++)
	Cards[i].Init(i,0,i*10,i*100); //����������� ����� ����� �����
  return Cards;
}
Card* ExtendedCardFactory::CreateCards(const int &size)
{
  Card* Cards = new Card[size];
  for(int i = 0; i < size; i++)
	Cards[i].Init(i,rand()%2,i*10,i*100); //��� 0 �� ������ �����, � ��� 1 �� ������� �����
  return Cards;
}
