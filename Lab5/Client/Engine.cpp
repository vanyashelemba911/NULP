#include "Engine.h"
#include "ThreadFoo.h"
DWORD _stdcall Reciver(void* p);
DWORD _stdcall Sender(void* p);

//=================================LogEngine=============================================
LogEngine* LogEngine::PrevInstance = 0;
LogEngine::LogEngine()
{
  this->Cards = 0;
  this->OpenedID = -1;
}
LogEngine::LogEngine( const GameSettings &Settings )
{
  this->Settings = Settings;
  this->Cards = 0;
  this->OpenedID = -1;
}
LogEngine::operator = ( LogEngine& a )
{
  a = *this->PrevInstance;
  this->OpenedID = -1;
}
LogEngine* LogEngine::getInstance()
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new LogEngine;
	 return PrevInstance;
   }
}
bool LogEngine::UseBonus(int id)
{
  return false;
}
bool LogEngine::Start( GameSettings &Settings , GLEngine_Interface *GLInterface , NLEngine_Interface* NLInterface )
{
  this->GLInterface =  GLInterface;
  this->NLInterface = NLInterface;
  this->Settings = Settings;
  this->CurrentTime = Settings.GetTime();
  this->CurrentHP = Settings.GetHealth();
  this->CurrentScore = 0;
  this->OpenedID = -1;
  this->Cards = new JustCard[this->Settings.GetCardsNum()];
  this->CurrCardsNum = this->Settings.GetCardsNum();
  CardTexture_T tmp;
  for(int i = 0; i < this->Settings.GetCardsNum(); i++)
  {
	tmp = GetRandom_CardTexture_T();
	Cards[i].Initialize(i,JUSTCARD,tmp);
	i++;
	Cards[i].Initialize(i,JUSTCARD,tmp);
  }
  JustCard TempCard;
  int r1;
  int r2;
  for(int i = 0; i < this->Settings.GetCardsNum(); i++)
  {
	r1 = rand()%this->Settings.GetCardsNum();
	r2 = rand()%this->Settings.GetCardsNum();
	while(r1==r2)r2 = rand()%this->Settings.GetCardsNum();
	TempCard = Cards[r1];
	Cards[r1] = Cards[r2];
	Cards[r2] = TempCard;
  }
  GEngineData gData;
  gData.Style = Settings.GetStyle();
  gData.NumberOfCards = Settings.GetCardsNum();
  gData.CardTextures = new CardTexture_T[gData.NumberOfCards];

  for(int i = 0; i < gData.NumberOfCards; i++)
  {
	gData.CardTextures[i] = this->Cards[i].GetCardTextureType();
  }
  this->GLInterface->StartGraphInit(gData);
  this->GLInterface->SendToGraphEng(DRAWTEXTURES,0);
  this->Timer = CreateThread(0,0,DecrementTime,this,0,0);
  LOG->Write("���������� ��� �����: ");
  for(int i = 0; i < this->Settings.GetCardsNum(); i++)
  {
	LOG->Write("  ����� �"+UnicodeString(i)+" = " + UnicodeString(int(this->Cards[i].GetCardTextureType())));
  }

  return false;
}
bool LogEngine::Quit()
{
  LOG->Write("LogEngine::Quit()");
  LOG->Write("CurrentHP: " + UnicodeString(this->CurrentHP));
  LOG->Write("CurrentScore: " + UnicodeString(this->CurrentScore));
  LOG->Write("CurrCardsNum: " + UnicodeString(this->CurrCardsNum));
  LOG->Write("CurrentTime: " + UnicodeString(this->CurrentTime));
  this->GLInterface->SendToGraphEng( GAMESTOP , this->CurrentScore );
  TimeOut();
  this->NLInterface->SubmitScore(this->Settings.GetUserNameW(),this->Settings.GetTime()-this->CurrentTime,this->CurrentScore,this->Settings.GetCardsNum());
  return false;
}

bool LogEngine::TimeOut()
{
  if( this->Timer )
  {
	TerminateThread(this->Timer,0);
	CloseHandle(this->Timer);
  }
  this->CurrentHP = 0;
  return true;
}

bool LogEngine::OpenCard(const int &id)
{
  if(this->OpenedID == -1)
  {
	this->OpenedID = id;
  }
  else
  {
	if(this->OpenedID != id)
	if( this->Cards[id].GetCardTextureType() == this->Cards[this->OpenedID].GetCardTextureType() )
	{
	  this->GLInterface->SendToGraphEng(DELETECARD,id);
	  this->GLInterface->SendToGraphEng(DELETECARD,OpenedID);
	  CurrCardsNum -= 2;
	  this->CurrentScore += this->Cards[id].GetDamage()*10;
	}
	else
	{
	  this->GLInterface->SendToGraphEng(HIDECARD,id);
	  this->GLInterface->SendToGraphEng(HIDECARD,OpenedID);
	  this->CurrentHP -= this->Cards[id].GetDamage();
	}
	this->OpenedID = -1;
  }
  if( CurrCardsNum <= 0 || this->CurrentHP <= 0 )
  {
    this->CurrentScore *= this->CurrentHP/10 * this->CurrentTime / 100;
	this->Quit();
  }
}

bool LogEngine::EventReact( const SomeEvent &SEvent , const int &id )
{
  switch(SEvent)
  {
	case SELECTCARD : this->OpenCard(id);
	//case GAMESTOP   : this->Quit();
  }
  return true;
}

DWORD _stdcall DecrementTime(void *P)
{
  LogEngine *ptr = (LogEngine*)(P);
  while( ptr->CurrentTime )
  {
	Sleep(1000);
	ptr->CurrentTime--;
  }
  ptr->CurrentHP = 0;
  ptr->TimeOut();
  return 0;
}
//=================================GraphEngine============================================
GraphEngine* GraphEngine::PrevInstance = 0;
GraphEngine::GraphEngine(TForm* Owner)
{
  this->Owner = Owner;
  Background  = new TImage(Owner);
  Background->Parent = Owner;
  Background->Picture->LoadFromFile("Textures\\Background.bmp");
  Background->Left = 0;
  Background->Width = Owner->Width;
  Background->Height = Owner->Height;
  Background->Top = 0;
  Background->Stretch = true;
  Background->Transparent = false;

  Panels  = new TImage(Owner);
  Panels->Picture->LoadFromFile("Textures\\Panels.bmp");
  Panels->Left = 0;
  Panels->Width = Owner->Width/5;
  Panels->Height = Owner->Height;
  Panels->Top = 0;
  Panels->Stretch = true;
  Panels->Transparent = false;
  Panels->Parent = Owner;
}
GraphEngine::operator = ( GraphEngine& a )
{
  a = *this->PrevInstance;
}
GraphEngine* GraphEngine::getInstance(TForm* Owner)
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new GraphEngine(Owner);
	 return PrevInstance;
   }
}
bool GraphEngine::StartMusic()
{
  this->MPlayer = new TMediaPlayer(this->Owner);
  this->MPlayer->Parent = this->Owner;
  this->MPlayer->Hide();
  MPlayer->FileName = "Music//Background.mp3";

  MPlayer->Open();
  MPlayer->Play();
}
bool GraphEngine::Start( GLEngine_Interface *GLInterface )
{
  this->StartMusic();
  this->GLInterface =  GLInterface;
  this->AnimationThread = CreateThread(0,0,InterfaceAnimation,this->Owner,0,0);
  return false;
}

bool GraphEngine::Stop()
{
  UITextures->Cleanup();
  delete this->l;
  delete this->Avatar;
  delete this->Health;
  delete this->TimeBar;
  delete this->ScoreLab;
  delete this->UITextures;
  this->UITextures = 0;
  if(this->AnimationThread)
  {
	TerminateThread(this->AnimationThread,0);
	CloseHandle(this->AnimationThread);
  }
  this->AnimationThread = 0;
  return 1;
}

bool GraphEngine::StartInit( const GEngineData &gData )
{
  this->gData = gData;
  RedTextureFactory RT_factory;
  DefTextureFactory DF_factory;
  if( this->gData.Style == 0 )
	UITextures = DF_factory.CreateTextures();
  else
	UITextures = RT_factory.CreateTextures();
  return 1;
}

bool GraphEngine::ShowCard(const int &id)
{
  if(!this->UITextures) return 0;
  this->UITextures->Show(id,gData.CardTextures[id]);
  this->Owner->Refresh();
  Sleep(250);
  this->GLInterface->SendToLogEng(SELECTCARD,id);
  return 1;
}

bool GraphEngine::HideCard(const int &id)
{
  this->UITextures->Hide(id);
  return 1;
}
bool GraphEngine::DeleteCard(const int &id)
{
  this->UITextures->Delete(id);
  return 1;
}
bool GraphEngine::ShowCongurlations(const int &Score)
{
  TLabel *CLabel = new TLabel(this->Owner);
  CLabel->Font->Height = 64;
  CLabel->Caption = "³����!! ���� ����: " + UnicodeString(Score);
  CLabel->Left = this->Owner->Width/2 - CLabel->Width/2 + this->Owner->Width/10 ;
  CLabel->Top = this->Owner->Height/2-this->Owner->Height/10;
  CLabel->Font->Color = clYellow;
  CLabel->Font->Style = CLabel->Font->Style<<fsBold;
  CLabel->Parent = this->Owner;
  this->Owner->Refresh();
  Sleep(2000);
  delete CLabel;
  this->MPlayer->Stop();
  this->MPlayer->Close();
  delete this->MPlayer;
  return 1;
}
bool GraphEngine::EventReact( const SomeEvent &SEvent , const int &id )
{
  switch(SEvent)
  {
	case DRAWTEXTURES : UITextures->Draw(Owner,gData); break;
	case SELECTCARD   : this->ShowCard(id); break;
	case HIDECARD     : this->HideCard(id); break;
	case DELETECARD   : this->DeleteCard(id); break;
	case GAMESTOP     : this->Stop();
						ShowCongurlations( id ); break;

  }
  return true;
}


DWORD _stdcall InterfaceAnimation(void *P)
{
  TForm *ptr = (TForm*)(P);
  LogEngine *LEptr = LogEngine::getInstance();
  GraphEngine *GEptr = GraphEngine::getInstance(ptr);
  int AvatarSize = 100;
  int HealthSize = 110;
  int Time = LEptr->CurrentTime;

  GEptr->Avatar = new TImage(ptr);
  GEptr->Avatar->Picture->LoadFromFile("Textures\\Avatar.bmp");
  GEptr->Avatar->Parent = ptr;
  GEptr->Avatar->Left = ptr->Width - AvatarSize - 15;
  GEptr->Avatar->Width = AvatarSize;
  GEptr->Avatar->Height = AvatarSize;
  GEptr->Avatar->Top = ptr->Height/15;
  GEptr->Avatar->Stretch = true;

  GEptr->Health = new TImage(ptr);
  GEptr->Health->Picture->LoadFromFile("Textures\\Health.bmp");
  GEptr->Health->Parent = ptr;
  GEptr->Health->Left = ptr->Width/5;
  GEptr->Health->Width = HealthSize;
  GEptr->Health->Height = HealthSize - 16;
  GEptr->Health->Top = ptr->Height/15;
  GEptr->Health->Transparent = true;

  GEptr->TimeBar = new TImage(ptr);
  GEptr->TimeBar->Picture->LoadFromFile("Textures\\TimeBar.bmp");
  GEptr->TimeBar->Parent = ptr;
  GEptr->TimeBar->Left = ptr->Width/5;
  GEptr->TimeBar->Width = ptr->Width - ptr->Width/5;
  GEptr->TimeBar->Height = ptr->Height/15;
  GEptr->TimeBar->Top = 0;
  GEptr->TimeBar->Stretch = true;

  GEptr->l = new TLabel(ptr);
  GEptr->l->Left = GEptr->Health->Left + GEptr->l->Width/2;
  GEptr->l->Top = GEptr->Health->Top + GEptr->l->Height*1.5;
  GEptr->l->Caption = LEptr->CurrentHP;
  GEptr->l->Font->Color = clLime;
  GEptr->l->Font->Height = 24;
  GEptr->l->Font->Style = GEptr->l->Font->Style<<fsBold;
  GEptr->l->Parent = ptr;

  GEptr->ScoreLab = new TLabel(ptr);
  GEptr->ScoreLab->Left = GEptr->Avatar->Left + GEptr->ScoreLab->Width/2+10;
  GEptr->ScoreLab->Top = GEptr->Avatar->Top + GEptr->Avatar->Height;
  GEptr->ScoreLab->Caption = LEptr->CurrentScore;
  GEptr->ScoreLab->Font->Color = clYellow;
  GEptr->ScoreLab->Font->Height = 30;
  GEptr->ScoreLab->Font->Style = GEptr->l->Font->Style<<fsBold;
  GEptr->ScoreLab->Parent = ptr;

  int TimeBarWidth = GEptr->TimeBar->Width;
  while( 1 )
  {
	Sleep(200);
	GEptr->TimeBar->Width = TimeBarWidth - (Time-LEptr->CurrentTime) * float(TimeBarWidth/Time);
	GEptr->l->Caption = LEptr->CurrentHP;
	GEptr->ScoreLab->Caption = LEptr->CurrentScore;
	GEptr->ScoreLab->Left = GEptr->Avatar->Left + GEptr->ScoreLab->Width/2+10;
  }
  LOG->Write("InterfaceAnimation() -> return 0;");
  return 0;
}

//===================================NetEngine==========================================
NetEngine* NetEngine::PrevInstance = 0;
NetEngine::NetEngine()
{
  ServerAddr = "";
  BufferIn = 0;
  BufferOut = 0;
  RThread = 0;
  SThread = 0;
  InitializeCriticalSection(&CS_Send);
  InitializeCriticalSection(&CS_Recive);
}
NetEngine::NetEngine( const NetData &NetworkData )
{
  ServerAddr = "";
  BufferIn = 0;
  BufferOut = 0;
  RThread = 0;
  SThread = 0;
  InitializeCriticalSection(&CS_Send);
  InitializeCriticalSection(&CS_Recive);
}
NetEngine::operator = ( NetEngine& a )
{
  a = *this->PrevInstance;
}
NetEngine* NetEngine::getInstance()
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new NetEngine;
	 return PrevInstance;
   }
}
bool NetEngine::Send(void* Data)
{
  LOG->Write("��������� ���� �� ������");
  EnterCriticalSection(&CS_Send);
  this->BufferOut = Data;
  NetFooData NData(CS_Send,this->BufferOut);
  LeaveCriticalSection(&CS_Send);
  this->SThread = CreateThread(0,0,Sender,(void*)&NData,0,0);
  return false;
}
void* NetEngine::Recive()
{
  LOG->Write("�������� ���� � �������");
  EnterCriticalSection(&CS_Recive);
  void* tmp = this->BufferIn;
  LeaveCriticalSection(&CS_Recive);
  return tmp;
}
bool NetEngine::StartEngine( NetData &NetworkData )
{
  LOG->Write("������ ����� ��������, �� ���� �� ��������");
  this->ServerAddr = NetworkData.geta();
  NetFooData NData(CS_Recive,this->BufferIn);
  this->RThread = CreateThread(0,0,Reciver,(void*)&NData,0,0);
  return false;
}
bool NetEngine::StopEngine( )
{
  LOG->Write("������ ����� ���������");
  TerminateThread(this->RThread,0);
  TerminateThread(this->SThread,0);
  DeleteCriticalSection(&this->CS_Send);
  DeleteCriticalSection(&this->CS_Recive);
  return false;
}

bool NetEngine::SubmitScore(UnicodeString Name, int Time, int Score, int CardsNum)
{
  HANDLE hNamedPipe = CreateFile(PipeNameForUser2, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
  STD_MESSAGE SomeMessage;
  SomeMessage = CreateMessage(AnsiString(Name.w_str()).c_str(),Time,Score,CardsNum);
  unsigned long HowMuch;
  void* pSomeMessage;
  pSomeMessage = (void*)&SomeMessage;
  LOG->Write("��������� �����������");
  LOG->Write("��������� ������:");
  LOG->Write("{");
  LOG->Write("NAME :" +UnicodeString(SomeMessage.UNAME));
  LOG->Write("TIME :" +UnicodeString(SomeMessage.TIME));
  LOG->Write("SCORE :"+UnicodeString(SomeMessage.SCORE));
  LOG->Write("CARDS :"+UnicodeString(SomeMessage.CARDS));
  LOG->Write("}");
  if( WriteFile(hNamedPipe, pSomeMessage, BUFER_SIZE, &HowMuch,NULL) )
  {
	LOG->Write("����������� ����������");
  }
  else
  {
	LOG->Write("����������� �� ����������, ������� ����������! ������� ����� �� ��������");
  }
  CloseHandle(hNamedPipe);
}

//==============================================================================
bool NLEngine_Interface::SubmitScore(UnicodeString Name, int Time, int Score, int CardsNum)
{
  this->NEptr->SubmitScore(Name,Time,Score,CardsNum);
}

NLEngine_Interface::NLEngine_Interface(LogEngine *LEptr, NetEngine *NEptr)
{
  this->LEptr = LEptr;
  this->NEptr = NEptr;
}

STD_MESSAGE CreateMessage(std::string Name, int Time, int Score, int CardsNum)
{
  STD_MESSAGE Msg;
  std::string tmp;
  char *bufer = new char[64];
  int i;
  for(i = 0; i < Name.size() && i < USERNAME_SIZE-1; i++)
  {
	Msg.UNAME[i] = Name[i];
  }
  Msg.UNAME[i] = '\0';
  tmp = std::itoa(Time,bufer,10);
  for(i = 0; i < tmp.size() && i < USERNAME_SIZE-1; i++)
  {
	Msg.TIME[i] = tmp[i];
  }
  Msg.TIME[i] = '\0';
  tmp = std::itoa(CardsNum,bufer,10);
  for(i = 0; i < tmp.size() && i < USERNAME_SIZE-1; i++)
  {
	Msg.CARDS[i] = tmp[i];
  }
  Msg.CARDS[i] = '\0';
  tmp = std::itoa(Score,bufer,10);
  for(i = 0; i < tmp.size() && i < USERNAME_SIZE-1; i++)
  {
	Msg.SCORE[i] = tmp[i];
  }
  Msg.SCORE[i] = '\0';
  delete [] bufer;
  return Msg;
}
void SetPipeName( UnicodeString st, wchar_t Name[] )
{
  for(int i = 1; i < 8; i++)
  {
	Name[i+9] = st[i];
  }
  Name[23] = st[st.Length()];
}

