#pragma once
#include "GlobalDeclarations.h"
#include "Gamemanager.h"
#include "DataStruct.h"

bool StartButton(  GameSettings *GS , GameManager *Manager , TForm* Owner);

bool OptionsButton();

bool ExitButton(GameManager *Manager);

bool LogButton(TMemo *ConsoleOut);
