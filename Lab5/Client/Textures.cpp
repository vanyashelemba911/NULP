#include "Textures.h"
#include "Engine.h"

TPicture* ImagesFactory::Card_Back = new TPicture;
TPicture* ImagesFactory::AppleCard = new TPicture;
TPicture* ImagesFactory::BananaCard = new TPicture;
TPicture* ImagesFactory::BonusScore = new TPicture;
TPicture* ImagesFactory::FreezeCard = new TPicture;
TPicture* ImagesFactory::GrapeCard = new TPicture;
TPicture* ImagesFactory::HummerCard = new TPicture;
TPicture* ImagesFactory::LemonCard = new TPicture;
TPicture* ImagesFactory::MinusScore = new TPicture;
TPicture* ImagesFactory::OrangeCard = new TPicture;
TPicture* ImagesFactory::PeachCard = new TPicture;
TPicture* ImagesFactory::PearCard = new TPicture;
TPicture* ImagesFactory::PineappleCard = new TPicture;
TPicture* ImagesFactory::PlumCard = new TPicture;
TPicture* ImagesFactory::ShieldCard = new TPicture;
TPicture* ImagesFactory::XrayCard = new TPicture;

TPicture* ImagesFactory::Get_AppleCard(const UnicodeString &st)
{
  if( AppleCard->Bitmap ) AppleCard->LoadFromFile("Textures\\"+st+"\\Cards\\AppleCard.bmp");
  return AppleCard;
}
TPicture* ImagesFactory::Get_Card_Back(const UnicodeString &st)
{
  if( Card_Back->Bitmap ) Card_Back->LoadFromFile("Textures\\"+st+"\\Cards\\Card_Back.bmp");
  return Card_Back;
}
TPicture* ImagesFactory::Get_BananaCard(const UnicodeString &st)
{
  if( BananaCard->Bitmap ) BananaCard->LoadFromFile("Textures\\"+st+"\\Cards\\BananaCard.bmp");
  return BananaCard;
}
TPicture* ImagesFactory::Get_BonusScore(const UnicodeString &st)
{
  if( BonusScore->Bitmap ) BonusScore->LoadFromFile("Textures\\"+st+"\\Cards\\BonusScore.bmp");
  return BonusScore;
}
TPicture* ImagesFactory::Get_FreezeCard(const UnicodeString &st)
{
  if( FreezeCard->Bitmap ) FreezeCard->LoadFromFile("Textures\\"+st+"\\Cards\\FreezeCard.bmp");
  return FreezeCard;
}
TPicture* ImagesFactory::Get_GrapeCard(const UnicodeString &st)
{
  if( GrapeCard->Bitmap ) GrapeCard->LoadFromFile("Textures\\"+st+"\\Cards\\GrapeCard.bmp");
  return GrapeCard;
}
TPicture* ImagesFactory::Get_HummerCard(const UnicodeString &st)
{
  if( HummerCard->Bitmap ) HummerCard->LoadFromFile("Textures\\"+st+"\\Cards\\HummerCard.bmp");
  return HummerCard;
}
TPicture* ImagesFactory::Get_LemonCard(const UnicodeString &st)
{
  if( LemonCard->Bitmap ) LemonCard->LoadFromFile("Textures\\"+st+"\\Cards\\LemonCard.bmp");
  return LemonCard;
}
TPicture* ImagesFactory::Get_MinusScore(const UnicodeString &st)
{
  if( MinusScore->Bitmap ) MinusScore->LoadFromFile("Textures\\"+st+"\\Cards\\MinusScore.bmp");
  return MinusScore;
}
TPicture* ImagesFactory::Get_OrangeCard(const UnicodeString &st)
{
  if( OrangeCard->Bitmap ) OrangeCard->LoadFromFile("Textures\\"+st+"\\Cards\\OrangeCard.bmp");
  return OrangeCard;
}
TPicture* ImagesFactory::Get_PeachCard(const UnicodeString &st)
{
  if( PeachCard->Bitmap ) PeachCard->LoadFromFile("Textures\\"+st+"\\Cards\\PeachCard.bmp");
  return PeachCard;
}
TPicture* ImagesFactory::Get_PearCard(const UnicodeString &st)
{
  if( PearCard->Bitmap ) PearCard->LoadFromFile("Textures\\"+st+"\\Cards\\PearCard.bmp");
  return PearCard;
}
TPicture* ImagesFactory::Get_PineappleCard(const UnicodeString &st)
{
  if( PineappleCard->Bitmap ) PineappleCard->LoadFromFile("Textures\\"+st+"\\Cards\\PineappleCard.bmp");
  return PineappleCard;
}
TPicture* ImagesFactory::Get_PlumCard(const UnicodeString &st)
{
  if( PlumCard->Bitmap ) PlumCard->LoadFromFile("Textures\\"+st+"\\Cards\\PlumCard.bmp");
  return PlumCard;
}
TPicture* ImagesFactory::Get_ShieldCard(const UnicodeString &st)
{
  if( ShieldCard->Bitmap ) ShieldCard->LoadFromFile("Textures\\"+st+"\\Cards\\ShieldCard.bmp");
  return ShieldCard;
}
TPicture* ImagesFactory::Get_XrayCard(const UnicodeString &st)
{
  if( XrayCard->Bitmap ) XrayCard->LoadFromFile("Textures\\"+st+"\\Cards\\XrayCard.bmp");
  return XrayCard;
}

//==============================================================================

RedTextures::~RedTextures()
{
  Cleanup();
}

bool RedTextures::Hide( const int &id )
{
  CardTexture[id]->Picture = ImagesFactory::Get_Card_Back("RedStyleIcons");
}

bool RedTextures::Delete( const int &id )
{
  CardTexture[id]->Hide();
}

bool RedTextures::Draw( TForm* Owner , const GEngineData &gData )
{
  CardTexture = new MyImageTexture*[gData.NumberOfCards];
  int l = Owner->Width/3,t = Owner->Height/5;
  int CardSize = 120;
  int AvatarSize = Owner->Width/15;
  NumOfCards = gData.NumberOfCards;
  for(int i = 0; i < gData.NumberOfCards; i++ )
  {
	CardTexture[i] = new MyImageTexture(Owner);
	CardTexture[i]->Picture = ImagesFactory::Get_Card_Back("RedStyleIcons");
	CardTexture[i]->Parent = Owner;
	CardTexture[i]->Left = l + (i%6)*CardSize + ((i%6)*CardSize)/3;
	CardTexture[i]->Width = CardSize;
	CardTexture[i]->Height = CardSize + CardSize/2;
	CardTexture[i]->Top = t + 1.75*((i/6)*CardSize);
	CardTexture[i]->Stretch = true;
	CardTexture[i]->Transparent = false;
	CardTexture[i]->Show();
	this->CardTexture[i]->id = i;
	this->CardTexture[i]->OnClick = &(this->CardTexture[i]->MyClick);
	this->CardTexture[i]->OnMouseEnter = &(this->CardTexture[i]->MyOnMouseEnter);
	this->CardTexture[i]->OnMouseLeave = &(this->CardTexture[i]->MyOnMouseLeave);
	this->CardTexture[i]->OwnerEngine = GraphEngine::getInstance(0);
  }
}
bool RedTextures::Cleanup()
{
  for(int i = 0; i < NumOfCards; i++ )
  {
	delete CardTexture[i];
  }
  delete [] CardTexture;
  CardTexture = 0;
}
bool DefTextures::Hide( const int &id )
{
  CardTexture[id]->Picture = ImagesFactory::Get_Card_Back("StandartIcons");
}

bool DefTextures::Delete( const int &id )
{
  CardTexture[id]->Hide();
}
bool DefTextures::Cleanup()
{
  for(int i = 0; i < NumOfCards; i++ )
  {
	delete CardTexture[i];
  }
  delete [] CardTexture;
  CardTexture = 0;
}
DefTextures::~DefTextures()
{
  Cleanup();
}

bool DefTextures::Draw( TForm* Owner , const GEngineData &gData )
{
  CardTexture = new MyImageTexture*[gData.NumberOfCards];
  int l = Owner->Width/3,t = Owner->Height/5;
  int CardSize = 120;
  int AvatarSize = Owner->Width/15;
  NumOfCards = gData.NumberOfCards;
  for(int i = 0; i < gData.NumberOfCards; i++ )
  {
	CardTexture[i] = new MyImageTexture(Owner);
	CardTexture[i]->Picture = ImagesFactory::Get_Card_Back("StandartIcons");
	CardTexture[i]->Parent = Owner;
	CardTexture[i]->Left = l + (i%6)*CardSize + ((i%6)*CardSize)/3;
	CardTexture[i]->Width = CardSize;
	CardTexture[i]->Height = CardSize + CardSize/2;
	CardTexture[i]->Top = t + 1.75*((i/6)*CardSize);
	CardTexture[i]->Stretch = true;
	CardTexture[i]->Transparent = false;
	CardTexture[i]->Show();
	this->CardTexture[i]->id = i;
	this->CardTexture[i]->OnClick = &(this->CardTexture[i]->MyClick);
	this->CardTexture[i]->OnMouseEnter = &(this->CardTexture[i]->MyOnMouseEnter);
	this->CardTexture[i]->OnMouseLeave = &(this->CardTexture[i]->MyOnMouseLeave);
	this->CardTexture[i]->OwnerEngine = GraphEngine::getInstance(0);
  }
}

bool DefTextures::Show( const int &id , CardTexture_T CT )
{
  switch(CT)
  {
	case T_AppleCard 	: CardTexture[id]->Picture = ImagesFactory::Get_AppleCard("StandartIcons"); break;
	case T_BananaCard 	: CardTexture[id]->Picture = ImagesFactory::Get_BananaCard("StandartIcons"); break;
	case T_BonusScore 	: CardTexture[id]->Picture = ImagesFactory::Get_BonusScore("StandartIcons"); break;
	case T_FreezeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_FreezeCard("StandartIcons"); break;
	case T_GrapeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_GrapeCard("StandartIcons"); break;
	case T_HummerCard 	: CardTexture[id]->Picture = ImagesFactory::Get_HummerCard("StandartIcons");  break;
	case T_LemonCard 	: CardTexture[id]->Picture = ImagesFactory::Get_LemonCard("StandartIcons"); break;
	case T_MinusScore 	: CardTexture[id]->Picture = ImagesFactory::Get_MinusScore("StandartIcons"); break;
	case T_OrangeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_OrangeCard("StandartIcons"); break;
	case T_PeachCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PeachCard("StandartIcons"); break;
	case T_PearCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PearCard("StandartIcons"); break;
	case T_PineappleCard: CardTexture[id]->Picture = ImagesFactory::Get_PineappleCard("StandartIcons"); break;
	case T_PlumCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PlumCard("StandartIcons"); break;
	case T_ShieldCard 	: CardTexture[id]->Picture = ImagesFactory::Get_ShieldCard("StandartIcons");  break;
	case T_XrayCard 	: CardTexture[id]->Picture = ImagesFactory::Get_XrayCard("StandartIcons"); break;
  }
}
bool DefTextures::CloseCard( const int &id )
{
  CardTexture[id]->Picture = ImagesFactory::Get_Card_Back("StandartIcons");
}

bool RedTextures::Show( const int &id , CardTexture_T CT)
{
  switch(CT)
  {
	case T_AppleCard 	: CardTexture[id]->Picture = ImagesFactory::Get_AppleCard("StandartIcons"); break;
	case T_BananaCard 	: CardTexture[id]->Picture = ImagesFactory::Get_BananaCard("StandartIcons"); break;
	case T_BonusScore 	: CardTexture[id]->Picture = ImagesFactory::Get_BonusScore("StandartIcons"); break;
	case T_FreezeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_FreezeCard("StandartIcons"); break;
	case T_GrapeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_GrapeCard("StandartIcons"); break;
	case T_HummerCard 	: CardTexture[id]->Picture = ImagesFactory::Get_HummerCard("StandartIcons");  break;
	case T_LemonCard 	: CardTexture[id]->Picture = ImagesFactory::Get_LemonCard("StandartIcons"); break;
	case T_MinusScore 	: CardTexture[id]->Picture = ImagesFactory::Get_MinusScore("StandartIcons"); break;
	case T_OrangeCard 	: CardTexture[id]->Picture = ImagesFactory::Get_OrangeCard("StandartIcons"); break;
	case T_PeachCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PeachCard("StandartIcons"); break;
	case T_PearCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PearCard("StandartIcons"); break;
	case T_PineappleCard: CardTexture[id]->Picture = ImagesFactory::Get_PineappleCard("StandartIcons"); break;
	case T_PlumCard 	: CardTexture[id]->Picture = ImagesFactory::Get_PlumCard("StandartIcons"); break;
	case T_ShieldCard 	: CardTexture[id]->Picture = ImagesFactory::Get_ShieldCard("StandartIcons");  break;
	case T_XrayCard 	: CardTexture[id]->Picture = ImagesFactory::Get_XrayCard("StandartIcons"); break;
  }
}
bool RedTextures::CloseCard( const int &id )
{
  CardTexture[id]->Picture = ImagesFactory::Get_Card_Back("RedStyleIcons");
}

void __fastcall MyImageTexture::MyClick(TObject* Sender)
{
  LOG->Write("this->CardTexture["+UnicodeString(this->id)+"]->OnClick = MyClick;");
  this->OwnerEngine->EventReact( SELECTCARD , this->id );
  return;
}

void __fastcall MyImageTexture::MyOnMouseEnter(TObject* Sender)
{
  int size = 20;
  this->Width += size;
  this->Height += size;
  this->Top -= size/2;
  this->Left -= size/2;
}
void __fastcall MyImageTexture::MyOnMouseLeave(TObject* Sender)
{
  int size = 20;
  this->Width -= size;
  this->Height -= size;
  this->Top += size/2;
  this->Left += size/2;
}
