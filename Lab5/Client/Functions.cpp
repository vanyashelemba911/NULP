#include "Functions.h"
//#include <vcl.h>

bool StartButton( GameSettings *GS , GameManager *Manager , TForm* Owner)
{
  if( Manager )
  {
	delete Manager;
  }
  Manager = new GameManager;
  Manager->Initialize( GS , Owner );
  Manager->Event(GAMESTART);
  return true;
}

bool OptionsButton()
{
  return true;
}

bool ExitButton(GameManager *Manager)
{
  if( Manager )
  {
	Manager->Event(GAMESTOP);
	return true;
  }
  return false;
}

bool LogButton(TMemo *ConsoleOut)
{
  LOG->GetLog(ConsoleOut);
  return true;
}
