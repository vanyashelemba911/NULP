#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"

class Card
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;
	CardTexture_T CardTextureType;
  public:
	virtual Card* Clone() = 0;
	virtual bool Initialize( const int &n, const CardType &CType , const CardTexture_T CardTextureType ) = 0;
	virtual int GetScoreBoost();
	virtual CardType GetCardType();
	virtual CardTexture_T GetCardTextureType();
};

class BonusCard : public Card
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;
	CardTexture_T CardTextureType;
  public:
	Card* Clone();
	bool Initialize( const int &n, const CardType &CType , const CardTexture_T CardTextureType );
};

class JustCard
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;
	CardTexture_T CardTextureType;
	int Damage;
  public:
	JustCard* Clone();
	bool Initialize( const int &n, const CardType &CType , const CardTexture_T CardTextureType );
	int GetDamage();
	CardTexture_T GetCardTextureType();
};

