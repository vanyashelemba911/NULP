#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"
#include "Entity.h"
#include "Textures.h"
#include "Interface.h"
#include "NetworkConsts.h"
#include <vcl.h>
#include <Vcl.MPlayer.hpp>
//--------------------------

DWORD _stdcall InterfaceAnimation(void *P);
DWORD _stdcall DecrementTime(void *P);
class GLEngine_Interface;
class NLEngine_Interface;

class LogEngine{
  private:
	int CurrentTime;
	int CurrentScore;
	int CurrentHP;
	int OpenedID;
	int CurrCardsNum;

	JustCard *Cards;
	GameSettings Settings;
	HANDLE Timer;
	GLEngine_Interface *GLInterface;
	NLEngine_Interface* NLInterface;

	bool SetBonus();
	bool InitializeCards();
	bool TimeOut();

	static LogEngine* PrevInstance;
	LogEngine();
	LogEngine( const GameSettings &Settings );
	operator =( LogEngine& );

	bool OpenCard(const int &id);
	bool UseBonus(int id);
  public:
	static LogEngine* getInstance();

	bool Start( GameSettings &Settings , GLEngine_Interface *GLInterface , NLEngine_Interface* NLInterface);
	bool Quit();
	bool EventReact( const SomeEvent &SEvent , const int &id );

	friend DWORD _stdcall DecrementTime(void *P);
	friend DWORD _stdcall InterfaceAnimation(void *P);
};

//--------------------------

class GraphEngine{
  private:
	SomeTextures *UITextures;
	TImage *Background;
	TImage *Panels;
	TImage* Avatar;
	TImage* Health;
	TImage* TimeBar;
	TLabel* l;
	TLabel* ScoreLab;
	TMediaPlayer* MPlayer;
	GEngineData gData;
	TForm* Owner;
	GLEngine_Interface *GLInterface;
	HANDLE AnimationThread;
	static GraphEngine* PrevInstance;

	GraphEngine(TForm* Owner);
	operator =( GraphEngine& );

	bool ShowCard(const int &id);
	bool HideCard(const int &id);
	bool DeleteCard(const int &id);
	bool ShowCongurlations(const int &Score);
	bool StartMusic();
  public:
	static GraphEngine* getInstance(TForm* Owner);
	bool Start( GLEngine_Interface *GLInterface );
	bool Stop();
	bool EventReact( const SomeEvent &SEvent , const int &id );
	bool StartInit( const GEngineData &gData );

	friend DWORD _stdcall InterfaceAnimation(void *P);
};

//--------------------------

class NetEngine{
  private:
	UnicodeString ServerAddr;
	void* BufferIn;
	void* BufferOut;
	HANDLE RThread;
	HANDLE SThread;
	static NetEngine* PrevInstance;
	NetEngine();
	NetEngine( const NetData &NetworkData );
	operator =( NetEngine& );
	CRITICAL_SECTION CS_Send;
	CRITICAL_SECTION CS_Recive;
  public:
	static NetEngine* getInstance();
	bool Send(void* Data);
	void* Recive();
	bool StartEngine( NetData &NetworkData );
	bool StopEngine( );
	bool SubmitScore(UnicodeString Name, int Time, int Score, int CardsNum);
};


class GLEngine_Interface
{
  private:
	void* Bufer;
	LogEngine *LEptr;
	GraphEngine *GEptr;

  public:
	GLEngine_Interface(LogEngine *LEptr, GraphEngine *GEptr);
	bool SendToLogEng(const SomeEvent &SEvent , const int &id);
	bool SendToGraphEng(const SomeEvent &SEvent , const int &id);
	bool StartGraphInit(const GEngineData &gData);
	//~GLEngine_Interface();
};

class NLEngine_Interface
{
  private:
	void* Bufer;
	LogEngine *LEptr;
	NetEngine *NEptr;
  public:
	NLEngine_Interface(LogEngine *LEptr, NetEngine *NEptr);
	bool SendToLogEng(SomeEvent Event);
	bool SendToNetEng(SomeEvent Event);
	bool SubmitScore(UnicodeString Name, int Time, int Score, int CardsNum);
	//~NLEngine_Interface();
};
