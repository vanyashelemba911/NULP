#pragma once
#include "NetworkConsts.h"
class Command
{
  public:
	virtual bool SubmitToServer(void *Command) = 0;
};

class ControlMessageCommand : public Command
{
  public:
	virtual bool SubmitToServer(void *Command);
};

class DataMessageCommand : public Command
{
  public:
	virtual bool SubmitToServer(void *Command);
};

class Sender
{
  public:
	bool Submit(Command *COM);
};

bool Sender::Submit(Command *COM)
{
  return COM.SubmitToServer();
}

DataMessageCommand::SubmitToServer(void *Command)
{
  HANDLE hNamedPipe = CreateFile(PipeNameForUser2, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
  STD_MESSAGE SomeMessage = *( (STD_MESSAGE*)(Command) );
  unsigned long HowMuch;
  void* pSomeMessage;
  pSomeMessage = (void*)&SomeMessage;
  LOG->Write("��������� �����������");
  LOG->Write("��������� ������:");
  LOG->Write("{");
  LOG->Write("NAME :" +UnicodeString(SomeMessage.UNAME));
  LOG->Write("TIME :" +UnicodeString(SomeMessage.TIME));
  LOG->Write("SCORE :"+UnicodeString(SomeMessage.SCORE));
  LOG->Write("CARDS :"+UnicodeString(SomeMessage.CARDS));
  LOG->Write("}");
  if( WriteFile(hNamedPipe, pSomeMessage, BUFER_SIZE, &HowMuch,NULL) )
  {
	LOG->Write("����������� ����������");
  }
  else
  {
	LOG->Write("����������� �� ����������, ������� ����������! ������� ����� �� ��������");
  }
  CloseHandle(hNamedPipe);
}

ControlMessageCommand::SubmitToServer(void *Command)
{
  HANDLE hNamedPipe = CreateFile(PipeNameForUser2, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
  CONTROL_STD_MESSAGE SomeMessage = *( (CONTROL_STD_MESSAGE*)(Command) );
  unsigned long HowMuch;
  void* pSomeMessage;
  pSomeMessage = (void*)&SomeMessage;
  LOG->Write("��������� ControlMessage");
  LOG->Write("��������� ������:");
  LOG->Write("{");
  LOG->Write("CommandID :" +UnicodeString(SomeMessage.CommandID));
  LOG->Write("}");
  if( WriteFile(hNamedPipe, pSomeMessage, BUFER_SIZE, &HowMuch,NULL) )
  {
	LOG->Write("����������� ����������");
  }
  else
  {
	LOG->Write("����������� �� ����������, ������� ����������! ������� ����� �� ��������");
  }
  CloseHandle(hNamedPipe);
}

