#include "DataStruct.h"
GameSettings::GameSettings(const int& Health, const int& Time,const int& Score, const UnicodeString& UserName, const int& CardsNum, const int& BonusCardsNum, const Difficulty& GameDiff , const int& Style)
{
  this->Health = Health;
  this->Time = Time;
  this->Score = Score;
  this->UserName = UserName;
  this->CardsNum = CardsNum;
  this->BonusCardsNum = BonusCardsNum;
  this->GameDiff = GameDiff;
  this->GameStyle = Style;
}

GameSettings::GameSettings()
{
  this->Health = 0;
  this->Time = 0;
  this->Score = 0;
  this->UserName = 0;
  this->CardsNum = 0;
  this->BonusCardsNum = 0;
  this->GameDiff = 0;
  this->GameStyle = 0;
}
bool GameSettings::Reinitialise(const int& Health, const int& Time,const int& Score, const UnicodeString& UserName, const int& CardsNum, const int& BonusCardsNum, const Difficulty& GameDiff , const int& Style)
{
  this->Health = Health;
  this->Time = Time;
  this->Score = Score;
  this->UserName = UserName;
  this->CardsNum = CardsNum;
  this->BonusCardsNum = BonusCardsNum;
  this->GameDiff = GameDiff;
  this->GameStyle = Style;
}
int GameSettings::GetHealth(){ return this->Health; }
int GameSettings::GetTime(){ return this->Time; }
int GameSettings::GetScore(){ return this->Score; }
int GameSettings::GetStyle(){ return this->GameStyle; }
int GameSettings::GetCardsNum(){ return this->CardsNum; }
int GameSettings::GetBonusCardsNum(){ return this->BonusCardsNum; }
Difficulty GameSettings::GetGameDiff(){ return this->GameDiff; }
UnicodeString GameSettings::GetUserName(){ return this->UserName; }

NetData::NetData(int a)
{
  this->a = a;
}
int NetData::geta()
{
  return a;
}

