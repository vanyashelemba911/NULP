#pragma once

struct CardInfo
{
  int CardID;
  int CardType;
  int CardScoreBoost;
  int CardDamage;
  CardInfo();
  CardInfo(int CardID,int CardType,int CardScoreBoost,int CardDamage);
};
//----------------
class Card
{
  private:
	int CardID;
	int CardType;
	int CardScoreBoost;
	int CardDamage;

  public:
	Card();
	Card(int CardID,int CardType,int CardScoreBoost,int CardDamage);
	CardInfo GetCardInfo();
	bool Init(int CardID,int CardType,int CardScoreBoost,int CardDamage);
	int GetCardType();
	int GetScoreBoost();
	int GetCardDamage();
	int GetCardID();
};
//----------------
struct State
{
  Card* Cards;
  int Size;
  State( int s , Card *C );
  State( );
};
//----------------
class Momento
{
  private:
	State st;
  public:
	Momento(const State &st);
	const State GetState();
};
//----------------
class CardDeck
{
  private:
	int ID;
	Card* Cards;
	int Size;
  public:
	CardDeck();
	CardDeck(const State &st);
	Card operator [](const int &iterator);

	Momento CreateMomento();
	bool ReturnState( Momento &PState );
	bool Serialization();

	bool CreateCards(const int& Size);
};

//==============================================================================


