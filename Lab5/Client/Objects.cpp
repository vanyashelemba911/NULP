#include "Objects.h"
#include <fstream>
using namespace std;
CardInfo::CardInfo()
{
  CardID = 0;
  CardType = 0;
  CardScoreBoost = 0;
  CardDamage = 0;
}
CardInfo::CardInfo(int ID,int Type,int Boost,int Damage)
{
  CardID = ID;
  CardType = Type;
  CardScoreBoost = Boost;
  CardDamage = Damage;
}
//----------------
State::State( int s , Card *C )
{
  Size = s; Cards = new Card[s];
  while(s) Cards[s] = C[s--];
}
State::State( )
{
  Size = 0;
  Cards = 0;
}
//----------------
Card::Card()
{
  this->CardID = 0;
  this->CardType = 0;
  this->CardScoreBoost = 0;
  this->CardDamage = 0;
}

Card::Card(int CardID,int CardType,int CardScoreBoost,int CardDamage)
{
  this->Init(CardID,CardType,CardScoreBoost,CardDamage);
}

CardInfo Card::GetCardInfo()
{
  return CardInfo(CardID,CardType,CardScoreBoost,CardDamage);
}

bool Card::Init(int CardID,int CardType,int CardScoreBoost,int CardDamage)
{
  this->CardID = CardID;
  this->CardType = CardType;
  this->CardScoreBoost = CardScoreBoost;
  this->CardDamage = CardDamage;
}

int Card::GetCardType()
{
  return this->CardType;
}

int Card::GetCardID()
{
  return this->CardID;
}

int Card::GetScoreBoost()
{
  return this->CardScoreBoost;
}

int Card::GetCardDamage()
{
  return this->CardDamage;
}
//----------------
Momento::Momento(const State &st)
{
  this->st = st;
}
const State Momento::GetState()
{
  return this->st;
}
//----------------

CardDeck::CardDeck()
{
  this->ID = 0;
  this->Cards = 0;
  this->Size = 0;
}

CardDeck::CardDeck(const State &st)
{
  this->ID = 1;
  this->Size = st.Size;
  this->Cards = new Card[Size];
  for(int i = 0; i < Size; i++)
	this->Cards[i] = st.Cards[i];
}

Card CardDeck::operator [](const int &iterator)
{
  return this->Cards[iterator];
}

Momento CardDeck::CreateMomento()
{
  return Momento( State(Size,Cards) );
}

bool CardDeck::ReturnState( Momento &PState )
{
  State st = PState.GetState();
  this->ID = 1;
  this->Size = st.Size;
  this->Cards = new Card[Size];
  for(int i = 0; i < Size; i++)
	this->Cards[i] = st.Cards[i];
  return true;
}

bool CardDeck::Serialization()
{
  ofstream File1;
  File1.open("Serialization.json");
  if( File1.is_open() )
  {
	File1<<"{\n\tCardDeck: \n\t{\n";
	  File1<<"\t\t \"ID\" : "<<this->ID<<"\n";
	  File1<<"\t\t \"Size\" : "<<this->Size<<"\n";
	  File1<<"\t\t \"Cards\" : [\n";
	  for(int i = 0; i < this->Size; i++)
	  {
		File1<<"\t\t\t \"Card #"<<i+1<<"\" : {\n";
		  File1<<"\t\t\t\t \"CardID\" : "<<this->Cards[i].GetCardID()<<"\n";
		  File1<<"\t\t\t\t \"ScoreBoost\" : "<<this->Cards[i].GetScoreBoost()<<"\n";
		  File1<<"\t\t\t\t \"CardDamage\" : "<<this->Cards[i].GetCardDamage()<<"\n";
		  File1<<"\t\t\t\t \"CardType\" : "<<this->Cards[i].GetCardType()<<"\n";
		File1<<"\t\t\t }\n";
	  }
	  File1<<"\t\t]\n";
	File1<<"\n\t}\n";
	File1<<"\n}";
  }
  return true;
}

bool CardDeck::CreateCards(const int& Size)
{
  this->ID = 1;
  this->Size = Size;
  this->Cards = new Card;
  for(int i = 0; i < Size; i++)
	this->Cards[i].Init(i,0,i*10,i*100);
}
//----------------
