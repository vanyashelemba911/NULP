#pragma once
#include "Objects.h"

class CardFactory
{
  private:
	virtual Card* CreateCards(const int &size) = 0;
  public:
	CardDeck GetCardDeck(const int &size);
};

class JustCardFactory : public CardFactory
{
  private:
	Card* CreateCards(const int &size);

};

class ExtendedCardFactory : public CardFactory
{
  private:
	Card* CreateCards(const int &size);
};


