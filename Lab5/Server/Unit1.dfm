object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Server'
  ClientHeight = 438
  ClientWidth = 802
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StringGrid1: TStringGrid
    Left = 0
    Top = 0
    Width = 535
    Height = 387
    ColCount = 4
    FixedCols = 0
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ColWidths = (
      129
      129
      130
      138)
  end
  object Button1: TButton
    Left = 0
    Top = 393
    Width = 225
    Height = 43
    Caption = 'Start'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 296
    Top = 393
    Width = 236
    Height = 43
    Caption = 'Stop'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Memo1: TMemo
    Left = 538
    Top = 0
    Width = 264
    Height = 438
    Align = alRight
    TabOrder = 3
  end
end
