//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

#pragma once
#include <vcl.h>

const unsigned long BUFER_SIZE = 64;
const unsigned long USERNAME_SIZE = 24;
const unsigned long SOMEDATA_SIZE = 10;
const unsigned long MESSAGE_SIZE = BUFER_SIZE - USERNAME_SIZE;
wchar_t PipeNameForServer[] 			= L"\\\\.\\pipe\\D\\$SomePipe1$";
wchar_t PipeNameForUser[] 				= L"\\\\Desktop-1opn2da\\pipe\\D\\$SomePipe1$";
wchar_t PipeNameForServer2[] 			= L"\\\\.\\pipe\\D\\$SomePipe2$";
wchar_t PipeNameForUser2[] 				= L"\\\\Desktop-1opn2da\\pipe\\D\\$SomePipe2$";
const unsigned long STD_WAIT_TIME = 10000;

struct STD_MESSAGE
{
  char UNAME[USERNAME_SIZE];
  char TIME[SOMEDATA_SIZE];
  char SCORE[SOMEDATA_SIZE];
  char CARDS[SOMEDATA_SIZE];
};

STD_MESSAGE CreateMessage(UnicodeString Name, int Time, int Score, int CardsNum)
{
  STD_MESSAGE Msg;
  char *UNAME = AnsiString(Name.w_str()).c_str();
  char *TIME = AnsiString(Time).c_str();
  char *SCORE = AnsiString(Score).c_str();
  char *CARDS = AnsiString(CardsNum).c_str();
  int L_TIME = AnsiString(Time).Length();
  int L_SCORE = AnsiString(Score).Length();
  int L_CARDS = AnsiString(CardsNum).Length();
  for(int i = 0; i < Name.Length(); i++)
  {
	Msg.UNAME[i] = UNAME[i];
  }
  for(int i = 0; i < L_TIME; i++)
  {
	Msg.TIME[i] = TIME[i];
  }
  for(int i = 0; i < L_SCORE; i++)
  {
	Msg.SCORE[i] = SCORE[i];
  }
  for(int i = 0; i < L_CARDS; i++)
  {
	Msg.CARDS[i] = CARDS[i];
  }
  return Msg;
}

void SetPipeName( UnicodeString st, wchar_t Name[] )
{
  for(int i = 1; i < 8; i++)
  {
	Name[i+9] = st[i];
  }
  Name[23] = st[st.Length()];
}

TForm1 *Form1;
HANDLE hNamedPipe;
HANDLE MainPipe;
HANDLE hMainThread;
DWORD _stdcall MainThread(void* P);


//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
  this->StringGrid1->Cells[0][0] = "Name";
  this->StringGrid1->Cells[1][0] = "Time";
  this->StringGrid1->Cells[2][0] = "Score";
  this->StringGrid1->Cells[3][0] = "Cards";
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  Form1->Memo1->Lines->Add("������ �������");
  hMainThread = CreateThread(0,0,MainThread,0,0,0);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
  Form1->Memo1->Lines->Add("������� �������");
  Form1->Memo1->Lines->Add("");
  Form1->Memo1->Lines->Add("============================");
  Form1->Memo1->Lines->Add("");
  TerminateThread(hMainThread,0);
  CloseHandle(hMainThread);
  CloseHandle(hNamedPipe);
  CloseHandle(MainPipe);
}
//---------------------------------------------------------------------------
DWORD _stdcall MainThread(void* P)
{
  Form1->Memo1->Lines->Add("��������� ����� ��\'����...");
  MainPipe = CreateNamedPipe(
	PipeNameForServer2,
	PIPE_ACCESS_DUPLEX,
	PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT,
	PIPE_UNLIMITED_INSTANCES,
	512, 512, STD_WAIT_TIME, NULL);
  if(MainPipe == INVALID_HANDLE_VALUE)
  {
	Form1->Memo1->Lines->Add("����� ��\'���� �� ��������!");
  }
  else
  {
	Form1->Memo1->Lines->Add("����� ��\'���� ��������!");
  }
  STD_MESSAGE SomeMessage;
  unsigned long HowMuch;
  void* pSomeMessage;
  pSomeMessage = (void*)&SomeMessage;
  int i = 1;
  Form1->Memo1->Lines->Add("���������� ��������� �����������");
  while(1)
  {
	if( ReadFile(MainPipe,pSomeMessage,BUFER_SIZE,&HowMuch,NULL) )
	{
	  DisconnectNamedPipe(MainPipe);
	  Form1->Memo1->Lines->Add("�������� ����������� �� ���");
	  Form1->StringGrid1->Cells[0][i] = SomeMessage.UNAME;
	  Form1->StringGrid1->Cells[1][i] = SomeMessage.TIME;
	  Form1->StringGrid1->Cells[2][i] = SomeMessage.SCORE;
	  Form1->StringGrid1->Cells[3][i] = SomeMessage.CARDS;
	  i++;
	  Form1->StringGrid1->RowCount++;
	  ConnectNamedPipe(MainPipe, NULL);
	}
  }
  return 0;
}