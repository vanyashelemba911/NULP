#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"
#include "Entity.h"
#include "Textures.h"
#include <vcl.h>
//--------------------------

class LogEngine{
  private:
    CardArray *Cards;
	int HP;
	int Time;
	int CurrentScore;
	bool SetBonus();
	static LogEngine* PrevInstance;
	LogEngine();
	LogEngine( const GameSettings &Settings );
	operator =( LogEngine& );
  public:
	static LogEngine* getInstance();
	bool OpenCard(int id);
	bool UseBonus(int id);
	bool Start( GameSettings &Settings );
	bool Quit();
	//~LogEngine();
};

//--------------------------

class GraphEngine{
  private:
	SomeTextures *UITextures;
	TImage *Background;
	TImage *Panels;
	GEngineData gData;
	TForm* Owner;

	static GraphEngine* PrevInstance;

	GraphEngine(TForm* Owner);
	operator =( GraphEngine& );

	bool Init();
  public:
	static GraphEngine* getInstance(TForm* Owner);
	bool Start( const GEngineData &gData );
	bool EventReact( const SomeEvent &SEvent );
};

//--------------------------

class NetEngine{
  private:
	UnicodeString ServerAddr;
	void* BufferIn;
	void* BufferOut;
	HANDLE RThread;
	HANDLE SThread;
	static NetEngine* PrevInstance;
	NetEngine();
	NetEngine( const NetData &NetworkData );
	operator =( NetEngine& );
	CRITICAL_SECTION CS_Send;
	CRITICAL_SECTION CS_Recive;
  public:
	static NetEngine* getInstance();
	bool Send(void* Data);
	void* Recive();
	bool StartEngine( NetData &NetworkData );
	bool StopEngine( );
};

