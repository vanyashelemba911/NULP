#pragma once
#include "GlobalDeclarations.h"
#include <vcl.h>
enum Difficulty { POTATO , EASY , NORMAL , HARD , GODMODE , WTF_MODE };
enum CardType   { JUSTCARD , BONUSCARD , WTFCARD , JOKER };
enum SomeEvent  { GAMESTART , GAMEFREZE , GAMESTOP , SELECTCARD , SELECTEFFECT };

struct NetFooData
{
  void* BufferIn;
  CRITICAL_SECTION cs;
  NetFooData(CRITICAL_SECTION &C , void* B) { BufferIn = B; cs = C; }
};
class GameSettings
{
  private:
	int Health;
	int Time;
	int Score;
	UnicodeString UserName;
	int CardsNum;
	int BonusCardsNum;
	Difficulty GameDiff;
  public:
	GameSettings(int Health, int Time,int Score,UnicodeString UserName,
	int CardsNum, int BonusCardsNum, Difficulty GameDiff);

	int GetHealth();
	int GetTime();
	int GetScore();
	int GetCardsNum();
	int GetBonusCardsNum();
	Difficulty GetGameDiff();
	UnicodeString GetUserName();
};

class User
{
  private:
	int ID;
	UnicodeString Name;
	int Score;
	UnicodeString Avatar; //Name of picture
  public:
	User(int ID,UnicodeString Name,int Score,UnicodeString Avatar);
	int GetID();
	UnicodeString GetName();
	int GetScore();
	UnicodeString GetAvatar();
};

struct GEngineData
{
  int Style;
  UnicodeString Path;
  UnicodeString DefaultPath;
  UnicodeString Avatar;
  int NumberOfCards;
  CardType *CardTypes;
  GEngineData();
  GEngineData(UnicodeString Path , int AvatarID,int NumberOfCards,CardType *CardTypes);
};

class SomeEffect
{
  private:
	char EffectID;
  public:
	SomeEffect();
	bool DoSmth();
};

class NetData
{
  private:
	int a;
  public:
	NetData(int a);
	int geta();
};
