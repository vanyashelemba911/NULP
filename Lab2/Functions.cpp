#include "Functions.h"
//#include <vcl.h>

bool StartButton(GameManager *Manager , TForm* Owner)
{
  if( Manager )
  {
	delete Manager;
  }
  Manager = new GameManager;
  GameSettings *GS = new GameSettings(10,10,10,"sss",2,1,POTATO);
  Manager->Initialize(GS , Owner);
  Manager->Event(GAMESTART);
  return true;
}

bool OptionsButton()
{
  return false;
}

bool ExitButton()
{
  return false;
}

bool LogButton(TMemo *ConsoleOut)
{
  LOG->GetLog(ConsoleOut);
  return true;
}
