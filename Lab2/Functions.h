#pragma once
#include "GlobalDeclarations.h"
#include "Gamemanager.h"
#include "DataStruct.h"

bool StartButton(GameManager *Manager , TForm* Owner);

bool OptionsButton();

bool ExitButton();

bool LogButton(TMemo *ConsoleOut);
