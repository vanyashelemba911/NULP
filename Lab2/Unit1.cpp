//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "GlobalDeclarations.h"
#include "Functions.h"
#include "Engine.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
GameManager *Manager;
ActionLog *LOG;

//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
  if(StartButton(Manager,Form1))
  {
	ShowMessage("All good!");
  }
  else
  {
	ShowMessage("Fail to start game!");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
  if(OptionsButton())
  {
	ShowMessage("All good!");
  }
  else
  {
	ShowMessage("Fail to open options!");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
  if(ExitButton())
  {
	ShowMessage("All good!");
  }
  else
  {
	ShowMessage("Fail to exit game!");
  }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
  this->Memo1->Show();
  LogButton(this->Memo1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormCreate(TObject *Sender)
{
  this->DoubleBuffered = true;
  LOG = new ActionLog;
  LOG->Write("����������� ������ �������...");
  GraphEngine* GEngine = GraphEngine::getInstance(Form1);
  this->Memo1->Hide();
}
//---------------------------------------------------------------------------

