#include "ActionLog.h"
#include <vcl.h>

bool ActionLog::Write( const UnicodeString& st )
{
  if(st.Length())
  {
	this->Log.push_back(UnicodeString(this->GetDateTime()) + UnicodeString(st));
	return true;
  }
  return false;
}

UnicodeString ActionLog::GetLastNote()
{
  return this->Log.back();
}

UnicodeString ActionLog::GetFirstNote()
{
  return this->Log.front();
}

bool ActionLog::GetLog(TMemo* Console)
{
  Console->Clear();
  std::list<UnicodeString> tmp;
  while(this->Log.size())
  {
	tmp.push_front(Log.back());
	Console->Lines->Add(Log.front());
	Log.pop_front();
  }
  while(tmp.size())
  {
	this->Log.push_back(tmp.front());
	tmp.pop_front();
  }
}

bool ActionLog::SaveToFile( const UnicodeString& Path )
{
  return true;
}

UnicodeString ActionLog::GetDateTime()
{
  return "[DATE] ";
}
