#pragma once
#include "GlobalDeclarations.h"

class GLEngine_Interface
{
  private:
	void* Bufer;
  public:
	GLEngine_Interface(LogEngine *LEptr, GraphEngine *GEptr);
	bool SendToLogEng(SomeEvent Event);
	bool SendToGraphEng(SomeEvent Event);
	//~GLEngine_Interface();
};

class NLEngine_Interface
{
  private:
	void* Bufer;
  public:
	NLEngine_Interface(LogEngine *LEptr, NetEngine *NEptr);
	bool SendToLogEng(SomeEvent Event);
	bool SendToNetEng(SomeEvent Event);
	//~NLEngine_Interface();
};
