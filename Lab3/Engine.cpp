#include "Engine.h"
#include "ThreadFoo.h"
DWORD _stdcall Reciver(void* p);
DWORD _stdcall Sender(void* p);

//=================================LogEngine=============================================
LogEngine* LogEngine::PrevInstance = 0;
LogEngine::LogEngine()
{
  this->HP = 0;
  this->CurrentScore = 0;
  this->Cards = 0;
}
LogEngine::LogEngine( const GameSettings &Settings )
{
  this->HP = 0;
  this->CurrentScore = 0;
  this->Cards = 0;
}
LogEngine::operator = ( LogEngine& a )
{
  a = *this->PrevInstance;
}
LogEngine* LogEngine::getInstance()
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new LogEngine;
	 return PrevInstance;
   }
}
bool LogEngine::OpenCard(int id)
{
  return false;
}
bool LogEngine::UseBonus(int id)
{
  return false;
}
bool LogEngine::Start( GameSettings &Settings )
{
  this->HP = Settings.GetHealth();
  this->CurrentScore = Settings.GetScore();
  this->Time = Settings.GetTime();
  this->Cards = new CardArray(Settings);
  return false;
}
bool LogEngine::Quit()
{
  return false;
}
//=================================GraphEngine============================================
GraphEngine* GraphEngine::PrevInstance = 0;
GraphEngine::GraphEngine(TForm* Owner)
{
  this->Owner = Owner;
  Background  = new TImage(Owner);
  Background->Picture->LoadFromFile("Textures\\Background.bmp");
  Background->Parent = Owner;
  Background->Left = 0;
  Background->Width = Owner->Width;
  Background->Height = Owner->Height;
  Background->Top = 0;
  Background->Stretch = true;
  Background->Transparent = true;
  Background->Show();

  Panels  = new TImage(Owner);
  Panels->Picture->LoadFromFile("Textures\\Panels.bmp");
  Panels->Parent = Owner;
  Panels->Left = 0;
  Panels->Width = Owner->Width/5;
  Panels->Height = Owner->Height;
  Panels->Top = 0;
  Panels->Stretch = true;
  Panels->Transparent = true;   Panels->Show();
}
GraphEngine::operator = ( GraphEngine& a )
{
  a = *this->PrevInstance;
}
GraphEngine* GraphEngine::getInstance(TForm* Owner)
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new GraphEngine(Owner);
	 return PrevInstance;
   }
}

bool GraphEngine::Init()
{
  RedTextureFactory RT_factory;
  DefTextureFactory DF_factory;
  gData.Style = 0;    //���� ����� ��� ����������
  if( this->gData.Style == 0 )
	UITextures = DF_factory.CreateTextures();
  else
	UITextures = RT_factory.CreateTextures();
  UITextures->Draw(Owner,gData);
}

bool GraphEngine::Start( const GEngineData &gData )
{
  this->gData = gData;
  this->Init();
  return false;
}
bool GraphEngine::EventReact( const SomeEvent &SEvent )
{
  return false;
}
//===================================NetEngine==========================================
NetEngine* NetEngine::PrevInstance = 0;
NetEngine::NetEngine()
{
  ServerAddr = "";
  BufferIn = 0;
  BufferOut = 0;
  RThread = 0;
  SThread = 0;
  InitializeCriticalSection(&CS_Send);
  InitializeCriticalSection(&CS_Recive);
}
NetEngine::NetEngine( const NetData &NetworkData )
{
  ServerAddr = "";
  BufferIn = 0;
  BufferOut = 0;
  RThread = 0;
  SThread = 0;
  InitializeCriticalSection(&CS_Send);
  InitializeCriticalSection(&CS_Recive);
}
NetEngine::operator = ( NetEngine& a )
{
  a = *this->PrevInstance;
}
NetEngine* NetEngine::getInstance()
{
   if(PrevInstance)
   {
	 LOG->Write("�������� ���������� Singleton, ������� �������� �� ������ ���������� ��/'���");
	 return PrevInstance;
   }
   else
   {
	 LOG->Write("����� ���������� Singleton, ��������� ��/'��� � ������� �� ����� ��������!");
	 PrevInstance = new NetEngine;
	 return PrevInstance;
   }
}
bool NetEngine::Send(void* Data)
{
  LOG->Write("��������� ���� �� ������");
  EnterCriticalSection(&CS_Send);
  this->BufferOut = Data;
  NetFooData NData(CS_Send,this->BufferOut);
  LeaveCriticalSection(&CS_Send);
  this->SThread = CreateThread(0,0,Sender,(void*)&NData,0,0);
  return false;
}
void* NetEngine::Recive()
{
  LOG->Write("�������� ���� � �������");
  EnterCriticalSection(&CS_Recive);
  void* tmp = this->BufferIn;
  LeaveCriticalSection(&CS_Recive);
  return tmp;
}
bool NetEngine::StartEngine( NetData &NetworkData )
{
  LOG->Write("������ ����� ��������, �� ���� �� ��������");
  this->ServerAddr = NetworkData.geta();
  NetFooData NData(CS_Recive,this->BufferIn);
  this->RThread = CreateThread(0,0,Reciver,(void*)&NData,0,0);
  return false;
}
bool NetEngine::StopEngine( )
{
  LOG->Write("������ ����� ���������");
  TerminateThread(this->RThread,0);
  TerminateThread(this->SThread,0);
  DeleteCriticalSection(&this->CS_Send);
  DeleteCriticalSection(&this->CS_Recive);
  return false;
}
//==============================================================================


