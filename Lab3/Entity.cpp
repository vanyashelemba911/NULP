#include "Entity.h"
Card* BonusCard::Clone()
{
  BonusCard *Ret = new BonusCard;
  Ret->Initialize(this->Number,this->CType);
  return Ret;
}
bool BonusCard::Initialize( int n, CardType CType )
{
  this->Number = n;
  this->CType = CType;
  this->ScoreBoost = 100;
}
int BonusCard::GetScoreBoost()
{
  return this->ScoreBoost;
}

Card* JustCard::Clone()
{
  JustCard *Ret = new JustCard;
  Ret->Initialize(this->Number,this->CType);
  return Ret;
}
bool JustCard::Initialize( int n, CardType CType )
{
  this->Number = n;
  this->CType = CType;
  this->ScoreBoost = 100;
  this->Damage = 10;
}
int JustCard::GetScoreBoost()
{
  return this->ScoreBoost;
}
int JustCard::GetDamage()
{
  return this->Damage;
}
