#pragma once
#include <vcl.h>
#include <list>
#include <vector>

class ActionLog
{
  private:
	std::list<UnicodeString> Log;
	UnicodeString GetDateTime();
  public:
	bool Write( const UnicodeString& st );
	UnicodeString GetLastNote();
	UnicodeString GetFirstNote();
	bool GetLog(TMemo* Console);
	bool SaveToFile( const UnicodeString& Path );
};
