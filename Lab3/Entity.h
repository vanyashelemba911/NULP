#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"

class Card
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;

  public:
	virtual Card* Clone() = 0;
	virtual bool Initialize( int n, CardType CType ) = 0;
	virtual int GetScoreBoost() = 0;
};

class BonusCard : public Card
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;

  public:
	Card* Clone();
	bool Initialize( int n, CardType CType );
	int GetScoreBoost();
};

class JustCard : public Card
{
  private:
	int Number;
	CardType CType;
	int ScoreBoost;
	int Damage;

  public:
	Card* Clone();
	bool Initialize( int n, CardType CType );
	int GetScoreBoost();
	int GetDamage();
};

class CardArray{
  private:
	Card **Cards;
  public:
	CardArray( GameSettings &Settings );
	Card* operator [](int i);
};


CardArray::CardArray( GameSettings &Settings )
{
  this->Cards = new Card*[Settings.GetCardsNum()];
  for(int i = 0; i < Settings.GetBonusCardsNum(); i++)
  {
	this->Cards[i]->Initialize(i , BONUSCARD );
  }
  for(int i = Settings.GetBonusCardsNum(); i < Settings.GetCardsNum()/2; i++)
  {
	this->Cards[i]->Initialize(i , JUSTCARD );
	this->Cards[i+Settings.GetCardsNum()/2] = this->Cards[i]->Clone();
  }
}
Card* CardArray::operator [](int i)
{
  return this->Cards[i];
}

