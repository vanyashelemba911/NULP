#include "Gamemanager.h"

GameManager::GameManager()
{
  this->LEngine = 0;
  this->GEngine = 0;
  this->NEngine = 0;
  this->UI_Thread = 0;
  this->GL_Thread = 0;
  this->NET_THREAD = 0;
}

bool GameManager::Initialize( GameSettings *Settings , TForm* Owner )
{
  this->Owner = Owner;
  this->Settings = Settings;
}

bool GameManager::Event( const SomeEvent &sEvent )
{
  //enum SomeEvent  { GAMESTART , GAMEFREZE , GAMESTOP , SELECTCARD , SELECTEFFECT };
  switch(sEvent)
  {
	GAMESTART   : return Start();
	GAMEFREZE   : return Freze();
	GAMESTOP    : return Stop();
	SELECTCARD  : return SelectCard();
	SELECTEFFECT: return SelectEffect();
  }
  return false;
}

bool GameManager::Start()
{
  LOG->Write("������� ���...");

  LOG->Write("����������� ������ �����...");
  this->LEngine = LogEngine::getInstance();

  LOG->Write("����������� ������ �������...");
  this->GEngine = GraphEngine::getInstance(this->Owner);
  //=====fix it=====
  CardType CardTypes[] = { JUSTCARD , JUSTCARD , JUSTCARD , JUSTCARD };
  //================
  GEngineData gData("//textures",0,4,CardTypes);
  this->GEngine->Start(gData);
  LOG->Write("����������� ������ �����...");
  this->NEngine = NetEngine::getInstance();
  /*NetData NData(1);
  this->NEngine->StartEngine(NData);
  void* a; a = (void*)(&NData);
  this->NEngine->Send(a); */
  return false;
}
bool GameManager::Freze()
{
  LOG->Write("Game paused");

  return false;
}
bool GameManager::Stop()
{
  LOG->Write("Stoping game");

  return false;
}
bool GameManager::SelectCard()
{
  LOG->Write("User select card");

  return false;
}
bool GameManager::SelectEffect()
{
  LOG->Write("User Select Effect");

  return false;
}


