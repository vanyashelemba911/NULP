#include "Textures.h"

TPicture* ImagesFactory::CardWith_Apple = new TPicture;
TPicture* ImagesFactory::CardWith_Star = new TPicture;
TPicture* ImagesFactory::CardWith_Coin = new TPicture;
TPicture* ImagesFactory::CardWith_Strawberry = new TPicture;
TPicture* ImagesFactory::CardWith_Moon = new TPicture;
TPicture* ImagesFactory::BackCard = new TPicture;

TPicture* ImagesFactory::GetCardWith_Apple(const UnicodeString &st)
{
  if( CardWith_Apple->Bitmap ) CardWith_Apple->LoadFromFile("Textures\\"+st+"\\Default.bmp");
  return CardWith_Apple;
}
TPicture* ImagesFactory::GetCardWith_Star(const UnicodeString &st)
{
  if( CardWith_Star->Bitmap ) CardWith_Star->LoadFromFile("Textures\\"+st+"\\Default.bmp");
  return CardWith_Star;
}
TPicture* ImagesFactory::GetCardWith_Coin(const UnicodeString &st)
{
  if( CardWith_Star->Bitmap ) CardWith_Star->LoadFromFile("Textures\\"+st+"\\Default.bmp");
  return CardWith_Star;
}
TPicture* ImagesFactory::GetCardWith_Strawberry(const UnicodeString &st)
{
  if( CardWith_Strawberry->Bitmap ) CardWith_Strawberry->LoadFromFile("Textures\\"+st+"\\Default.bmp");
  return CardWith_Strawberry;
}
TPicture* ImagesFactory::GetCardWith_Moon(const UnicodeString &st)
{
  if( CardWith_Moon->Bitmap ) CardWith_Moon->LoadFromFile("Textures\\"+st+"\\Default.bmp");
  return CardWith_Moon;
}

bool RedTextures::Draw( TForm* Owner , const GEngineData &gData )
{
  CardTexture = new TImage*[gData.NumberOfCards];
  int l = Owner->Width/5,t = Owner->Height/5;
  int CardSize = 70;
  int AvatarSize = Owner->Width/15;

  Avatar = new TImage(Owner);
  Avatar->Picture->LoadFromFile("Textures\\RedStyleIcons\\Avatar.bmp");
  Avatar->Parent = Owner;
  Avatar->Left = Owner->Width - AvatarSize - 15;
  Avatar->Width = AvatarSize;
  Avatar->Height = AvatarSize;
  Avatar->Top = Owner->Height/20;
  Avatar->Stretch = true;
  Avatar->Transparent = false;
  Avatar->Show();

  for(int i = 0; i < gData.NumberOfCards; i++ )
  {
	CardTexture[i] = new TImage(Owner);
	CardTexture[i]->Picture = ImagesFactory::GetCardWith_Apple("RedStyleIcons");
	CardTexture[i]->Parent = Owner;
	CardTexture[i]->Left = l + (i%4)*CardSize + 10;
	CardTexture[i]->Width = 50;
	CardTexture[i]->Height = 50;
	CardTexture[i]->Top = t + (i/4)*CardSize + 10;
	CardTexture[i]->Stretch = true;
	CardTexture[i]->Transparent = false;
	CardTexture[i]->Show();
  }

  HelthBar = new TImage(Owner);
  HelthBar->Picture->LoadFromFile("Textures\\RedStyleIcons\\Health.bmp");
  HelthBar->Parent = Owner;
  HelthBar->Left = l;
  HelthBar->Width = Owner->Width-l;
  HelthBar->Height = Owner->Height/20;
  HelthBar->Top = 0;
  HelthBar->Stretch = true;
  HelthBar->Show();
}

bool DefTextures::Draw( TForm* Owner , const GEngineData &gData )
{
  CardTexture = new TImage*[gData.NumberOfCards];
  int l = Owner->Width/5,t = Owner->Height/5;
  int CardSize = 70;
  int AvatarSize = Owner->Width/15;
  for(int i = 0; i < gData.NumberOfCards; i++ )
  {
	CardTexture[i] = new TImage(Owner);
	CardTexture[i]->Picture = ImagesFactory::GetCardWith_Apple("StandartIcons");
	CardTexture[i]->Parent = Owner;
	CardTexture[i]->Left = l + (i%4)*CardSize + 10;
	CardTexture[i]->Width = 50;
	CardTexture[i]->Height = 50;
	CardTexture[i]->Top = t + (i/4)*CardSize + 10;
	CardTexture[i]->Stretch = true;
	CardTexture[i]->Transparent = false;
	CardTexture[i]->Show();
  }

  Avatar = new TImage(Owner);
  Avatar->Picture->LoadFromFile("Textures\\StandartIcons\\Avatar.bmp");
  Avatar->Parent = Owner;
  Avatar->Left = Owner->Width - AvatarSize - 15;
  Avatar->Width = AvatarSize;
  Avatar->Height = AvatarSize;
  Avatar->Top = Owner->Height/20;
  Avatar->Stretch = true;
  Avatar->Transparent = false;
  Avatar->Show();

  HelthBar = new TImage(Owner);
  HelthBar->Picture->LoadFromFile("Textures\\StandartIcons\\Health.bmp");
  HelthBar->Parent = Owner;
  HelthBar->Left = l;
  HelthBar->Width = Owner->Width-l;
  HelthBar->Height = Owner->Height/20;
  HelthBar->Top = 0;
  HelthBar->Stretch = true;
  HelthBar->Show();
}

