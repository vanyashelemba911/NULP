#pragma once
#include "GlobalDeclarations.h"
#include "Engine.h"

class GameManager
{
  private:
	LogEngine* LEngine;
	NetEngine* NEngine;
	GraphEngine* GEngine;
	HANDLE UI_Thread;
	HANDLE GL_Thread;
	HANDLE NET_THREAD;
	GameSettings *Settings;
	TForm* Owner;
	bool Start();
	bool Freze();
	bool Stop();
	bool SelectCard();
	bool SelectEffect();
  public:
	GameManager();
	bool Initialize( GameSettings *Settings , TForm* Owner );
	bool Event( const SomeEvent &sEvent );
};