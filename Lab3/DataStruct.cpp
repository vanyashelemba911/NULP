#include "DataStruct.h"


GameSettings::GameSettings(int Health, int Time,int Score,UnicodeString UserName,
	int CardsNum, int BonusCardsNum, Difficulty GameDiff)
{
  this->Health = Health;
  this->Time = Time;
  this->Score = Score;
  this->UserName = UserName;
  this->CardsNum = CardsNum;
  this->BonusCardsNum = BonusCardsNum;
  this->GameDiff = GameDiff;
}

int GameSettings::GetHealth(){ return this->Health; }
int GameSettings::GetTime(){ return this->Time; }
int GameSettings::GetScore(){ return this->Score; }
int GameSettings::GetCardsNum(){ return this->CardsNum; }
int GameSettings::GetBonusCardsNum(){ return this->BonusCardsNum; }
Difficulty GameSettings::GetGameDiff(){ return this->GameDiff; }
UnicodeString GameSettings::GetUserName(){ return this->UserName; }

NetData::NetData(int a)
{
  this->a = a;
}
int NetData::geta()
{
  return a;
}

GEngineData::GEngineData( )
{
  Path = "//Textures";
  DefaultPath = "//Textures";
}

GEngineData::GEngineData(UnicodeString P , int Avatar,int Number,CardType *Types)
{
   Path = "//Textures";
   DefaultPath = "//Textures";
   Avatar = Avatar;
   NumberOfCards = Number;
   CardTypes = Types;
}

