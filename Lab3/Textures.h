#pragma once
#include "GlobalDeclarations.h"
#include "DataStruct.h"
#include <vcl.h>

class ImagesFactory{
  private:
	static TPicture* BackCard;
	static TPicture* CardWith_Apple;
	static TPicture* CardWith_Star;
	static TPicture* CardWith_Coin;
	static TPicture* CardWith_Strawberry;
	static TPicture* CardWith_Moon;
	ImagesFactory();
  public:
	static TPicture* GetBackCard(const UnicodeString &st);
	static TPicture* GetCardWith_Apple(const UnicodeString &st);
	static TPicture* GetCardWith_Star(const UnicodeString &st);
	static TPicture* GetCardWith_Coin(const UnicodeString &st);
	static TPicture* GetCardWith_Strawberry(const UnicodeString &st);
	static TPicture* GetCardWith_Moon(const UnicodeString &st);
};

class SomeTextures
{
  public:
	virtual bool Draw( TForm* Owner , const GEngineData &gData ) = 0;
};


class DefTextures : public SomeTextures
{
  TImage **CardTexture;
	TImage *Avatar;
	TImage *HelthBar;
	TImage *Effects;
  public:
	bool Draw( TForm* Owner , const GEngineData &gData );
};

class RedTextures : public SomeTextures
{
    TImage **CardTexture;
	TImage *Avatar;
	TImage *HelthBar;
	TImage *Effects;
  public:
	bool Draw( TForm* Owner , const GEngineData &gData );
};

class TextureFactory
{
  public:
	virtual SomeTextures* CreateTextures() = 0;
};

class RedTextureFactory : public TextureFactory
{
  public:
  SomeTextures* CreateTextures()
  {
	return new RedTextures;
  }
};

class DefTextureFactory : public TextureFactory
{
  public:
  SomeTextures* CreateTextures()
  {
	return new DefTextures;
  }
};
